; * ---------------------------------------------------------------------------------------
; *  @file:    startup_S32K144.s
; *  @purpose: CMSIS Cortex-M4 Core Device Startup File
; *            S32K144
; *  @version: 2.0
; *  @date:    2017-1-10
; *  @build:   b170107
; * ---------------------------------------------------------------------------------------
; *
; * Copyright (c) 1997 - 2016 , Freescale Semiconductor, Inc.
; * Copyright 2016-2017 NXP
; * All rights reserved.
; *
; * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
; * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
; * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
; * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
; * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
; * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
; * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
; * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
; * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
; * THE POSSIBILITY OF SUCH DAMAGE.
; *
; *------- <<< Use Configuration Wizard in Context Menu >>> ------------------
; *
; *****************************************************************************/


                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset

                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size
                IMPORT  |Image$$ARM_LIB_STACK$$ZI$$Limit|
				IMPORT  OSInterruptDispatcher
				IMPORT  OSNmiException
				IMPORT  OSHardFaultException
				IMPORT  OSBusFaultException
				IMPORT  OSUsageFaultException
				IMPORT  OSSVCallException
				IMPORT  OSDebugMonitorException
				IMPORT  OSPendSVException
				IMPORT  OSReservedException


__Vectors       DCD     |Image$$ARM_LIB_STACK$$ZI$$Limit| ; Top of Stack
                DCD     Reset_Handler  ; Reset Handler
                DCD     OSNmiException                      ;NMI Handler
                DCD     OSHardFaultException                ;Hard Fault Handler
                DCD     OSReservedException                 ;MPU Fault Handler
                DCD     OSBusFaultException                 ;Bus Fault Handler
                DCD     OSUsageFaultException               ;Usage Fault Handler
                DCD     OSReservedException                 ;Reserved
                DCD     OSReservedException                 ;Reserved
                DCD     OSReservedException                 ;Reserved
                DCD     OSReservedException                 ;Reserved
                DCD     OSSVCallException                   ;SVCall Handler
                DCD     OSDebugMonitorException             ;Debug Monitor Handler
                DCD     OSReservedException                 ;Reserved
                DCD     OSPendSVException                   ;PendSV Handler
                DCD     OSInterruptDispatcher               ;SysTick Handler

                ;External Interrupts
                DCD     OSInterruptDispatcher     ;DMA channel 0 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 1 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 2 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 3 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 4 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 5 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 6 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 7 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 8 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 9 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 10 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 11 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 12 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 13 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 14 transfer complete
                DCD     OSInterruptDispatcher     ;DMA channel 15 transfer complete

				        ;External interrupts 32 -
                DCD     OSInterruptDispatcher     ;DMA error interrupt channels 0-15
                DCD     OSInterruptDispatcher     ;FPU sources
                DCD     OSInterruptDispatcher     ;FTFC Command complete
                DCD     OSInterruptDispatcher     ;FTFC Read collision
                DCD     OSInterruptDispatcher     ;PMC Low voltage detect interrupt
                DCD     OSInterruptDispatcher     ;FTFC Double bit fault detect
                DCD     OSInterruptDispatcher     ;Single interrupt vector for WDOG and EWM
                DCD     OSInterruptDispatcher     ;RCM Asynchronous Interrupt
                DCD     OSInterruptDispatcher     ;LPI2C0 Master Interrupt
                DCD     OSInterruptDispatcher     ;LPI2C0 Slave Interrupt
                DCD     OSInterruptDispatcher     ;LPSPI0 Interrupt
                DCD     OSInterruptDispatcher     ;LPSPI1 Interrupt
                DCD     OSInterruptDispatcher     ;LPSPI2 Interrupt
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 45
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 46
                DCD     OSInterruptDispatcher     ;LPUART0 Transmit / Receive Interrupt

				        ;External interrupts 48 -
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 48
                DCD     OSInterruptDispatcher     ;LPUART1 Transmit / Receive  Interrupt
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 50
                DCD     OSInterruptDispatcher     ;LPUART2 Transmit / Receive  Interrupt
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 52
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 53
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 54
                DCD     OSInterruptDispatcher     ;ADC0 interrupt request.
                DCD     OSInterruptDispatcher     ;ADC1 interrupt request.
                DCD     OSInterruptDispatcher     ;CMP0 interrupt request
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 58
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 59
                DCD     OSInterruptDispatcher     ;ERM single bit error correction
                DCD     OSInterruptDispatcher     ;ERM double bit error non-correctable
                DCD     OSInterruptDispatcher     ;RTC alarm interrupt
                DCD     OSInterruptDispatcher     ;RTC seconds interrupt

				        ;External interrupts 64 -
                DCD     OSInterruptDispatcher     ;LPIT0 channel 0 overflow interrupt
                DCD     OSInterruptDispatcher     ;LPIT0 channel 1 overflow interrupt
                DCD     OSInterruptDispatcher     ;LPIT0 channel 2 overflow interrupt
                DCD     OSInterruptDispatcher     ;LPIT0 channel 3 overflow interrupt
                DCD     OSInterruptDispatcher     ;PDB0 interrupt
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 69
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 70
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 71
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 72
                DCD     OSInterruptDispatcher     ;SCG bus interrupt request
                DCD     OSInterruptDispatcher     ;LPTIMER interrupt request
                DCD     OSInterruptDispatcher     ;Port A pin detect interrupt
                DCD     OSInterruptDispatcher     ;Port B pin detect interrupt
                DCD     OSInterruptDispatcher     ;Port C pin detect interrupt
                DCD     OSInterruptDispatcher     ;Port D pin detect interrupt
                DCD     OSInterruptDispatcher     ;Port E pin detect interrupt

				        ;External interrupts 80 -
                DCD     OSInterruptDispatcher     ;Software interrupt
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 81
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 82
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 83
                DCD     OSInterruptDispatcher     ;PDB1 interrupt
                DCD     OSInterruptDispatcher     ;FlexIO Interrupt
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 86
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 87
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 88
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 89
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 90
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 91
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 92
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 93
                DCD     OSInterruptDispatcher     ;CAN0 OR'ed [Bus Off OR Transmit Warning OR Receive Warning]
                DCD     OSInterruptDispatcher     ;CAN0 Interrupt indicating that errors were detected on the CAN bus

				        ;External interrupts 96 -
                DCD     OSInterruptDispatcher     ;CAN0 Interrupt asserted when Pretended Networking operation is enabled, and a valid message matches the selected filter criteria during Low Power mode
                DCD     OSInterruptDispatcher     ;CAN0 OR'ed Message buffer (0-15)
                DCD     OSInterruptDispatcher     ;CAN0 OR'ed Message buffer (16-31)
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 99
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 100
                DCD     OSInterruptDispatcher     ;CAN1 OR'ed [Bus Off OR Transmit Warning OR Receive Warning]
                DCD     OSInterruptDispatcher     ;CAN1 Interrupt indicating that errors were detected on the CAN bus
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 103
                DCD     OSInterruptDispatcher     ;CAN1 OR'ed Interrupt for Message buffer (0-15)
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 105
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 106
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 107
                DCD     OSInterruptDispatcher     ;CAN2 OR'ed [Bus Off OR Transmit Warning OR Receive Warning]
                DCD     OSInterruptDispatcher     ;CAN2 Interrupt indicating that errors were detected on the CAN bus
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 110
                DCD     OSInterruptDispatcher     ;CAN2 OR'ed Message buffer (0-15)

				        ;External interrupts 112
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 112
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 113
                DCD     OSInterruptDispatcher     ;Reserved Interrupt 114
                DCD     OSInterruptDispatcher     ;FTM0 Channel 0 and 1 interrupt
                DCD     OSInterruptDispatcher     ;FTM0 Channel 2 and 3 interrupt
                DCD     OSInterruptDispatcher     ;FTM0 Channel 4 and 5 interrupt
                DCD     OSInterruptDispatcher     ;FTM0 Channel 6 and 7 interrupt
                DCD     OSInterruptDispatcher     ;FTM0 Fault interrupt
                DCD     OSInterruptDispatcher     ;FTM0 Counter overflow and Reload interrupt
                DCD     OSInterruptDispatcher     ;FTM1 Channel 0 and 1 interrupt
                DCD     OSInterruptDispatcher     ;FTM1 Channel 2 and 3 interrupt
                DCD     OSInterruptDispatcher     ;FTM1 Channel 4 and 5 interrupt
                DCD     OSInterruptDispatcher     ;FTM1 Channel 6 and 7 interrupt
                DCD     OSInterruptDispatcher     ;FTM1 Fault interrupt
                DCD     OSInterruptDispatcher     ;FTM1 Counter overflow and Reload interrupt
                DCD     OSInterruptDispatcher     ;FTM2 Channel 0 and 1 interrupt

				        ;External interrupts 128
                DCD     OSInterruptDispatcher     ;FTM2 Channel 2 and 3 interrupt
                DCD     OSInterruptDispatcher     ;FTM2 Channel 4 and 5 interrupt
                DCD     OSInterruptDispatcher     ;FTM2 Channel 6 and 7 interrupt
                DCD     OSInterruptDispatcher     ;FTM2 Fault interrupt
                DCD     OSInterruptDispatcher     ;FTM2 Counter overflow and Reload interrupt
                DCD     OSInterruptDispatcher     ;FTM3 Channel 0 and 1 interrupt
                DCD     OSInterruptDispatcher     ;FTM3 Channel 2 and 3 interrupt
                DCD     OSInterruptDispatcher     ;FTM3 Channel 4 and 5 interrupt
                DCD     OSInterruptDispatcher     ;FTM3 Channel 6 and 7 interrupt
                DCD     OSInterruptDispatcher     ;FTM3 Fault interrupt
                DCD     OSInterruptDispatcher     ;FTM3 Counter overflow and Reload interrupt
                DCD     OSInterruptDispatcher     ;139
                DCD     OSInterruptDispatcher     ;140
                DCD     OSInterruptDispatcher     ;141
                DCD     OSInterruptDispatcher     ;142
                DCD     OSInterruptDispatcher     ;143

				        ;External interrupts 144
                DCD     OSInterruptDispatcher     ;144
                DCD     OSInterruptDispatcher     ;145
                DCD     OSInterruptDispatcher     ;146
                DCD     OSInterruptDispatcher     ;147
                DCD     OSInterruptDispatcher     ;148
                DCD     OSInterruptDispatcher     ;149
                DCD     OSInterruptDispatcher     ;150
                DCD     OSInterruptDispatcher     ;151
                DCD     OSInterruptDispatcher     ;152
                DCD     OSInterruptDispatcher     ;153
                DCD     OSInterruptDispatcher     ;154
                DCD     OSInterruptDispatcher     ;155
                DCD     OSInterruptDispatcher     ;156
                DCD     OSInterruptDispatcher     ;157
                DCD     OSInterruptDispatcher     ;158
                DCD     OSInterruptDispatcher     ;159

				        ;External interrupts 160
                DCD     OSInterruptDispatcher     ;160
                DCD     OSInterruptDispatcher     ;161
                DCD     OSInterruptDispatcher     ;162
                DCD     OSInterruptDispatcher     ;163
                DCD     OSInterruptDispatcher     ;164
                DCD     OSInterruptDispatcher     ;165
                DCD     OSInterruptDispatcher     ;166
                DCD     OSInterruptDispatcher     ;167
                DCD     OSInterruptDispatcher     ;168
                DCD     OSInterruptDispatcher     ;169
                DCD     OSInterruptDispatcher     ;170
                DCD     OSInterruptDispatcher     ;171
                DCD     OSInterruptDispatcher     ;172
                DCD     OSInterruptDispatcher     ;173
                DCD     OSInterruptDispatcher     ;174
                DCD     OSInterruptDispatcher     ;175


                DCD     OSReservedException                          ;176
                DCD     OSReservedException                          ;177
                DCD     OSReservedException                          ;178
                DCD     OSReservedException                          ;179
                DCD     OSReservedException                          ;180
                DCD     OSReservedException                          ;181
                DCD     OSReservedException                          ;182
                DCD     OSReservedException                          ;183
                DCD     OSReservedException                          ;184
                DCD     OSReservedException                          ;185
                DCD     OSReservedException                          ;186
                DCD     OSReservedException                          ;187
                DCD     OSReservedException                          ;188
                DCD     OSReservedException                          ;189
                DCD     OSReservedException                          ;190
                DCD     OSReservedException                          ;191
                DCD     OSReservedException                          ;192
                DCD     OSReservedException                          ;193
                DCD     OSReservedException                          ;194
                DCD     OSReservedException                          ;195
                DCD     OSReservedException                          ;196
                DCD     OSReservedException                          ;197
                DCD     OSReservedException                          ;198
                DCD     OSReservedException                          ;199
                DCD     OSReservedException                          ;200
                DCD     OSReservedException                          ;201
                DCD     OSReservedException                          ;202
                DCD     OSReservedException                          ;203
                DCD     OSReservedException                          ;204
                DCD     OSReservedException                          ;205
                DCD     OSReservedException                          ;206
                DCD     OSReservedException                          ;207
                DCD     OSReservedException                          ;208
                DCD     OSReservedException                          ;209
                DCD     OSReservedException                          ;210
                DCD     OSReservedException                          ;211
                DCD     OSReservedException                          ;212
                DCD     OSReservedException                          ;213
                DCD     OSReservedException                          ;214
                DCD     OSReservedException                          ;215
                DCD     OSReservedException                          ;216
                DCD     OSReservedException                          ;217
                DCD     OSReservedException                          ;218
                DCD     OSReservedException                          ;219
                DCD     OSReservedException                          ;220
                DCD     OSReservedException                          ;221
                DCD     OSReservedException                          ;222
                DCD     OSReservedException                          ;223
                DCD     OSReservedException                          ;224
                DCD     OSReservedException                          ;225
                DCD     OSReservedException                          ;226
                DCD     OSReservedException                          ;227
                DCD     OSReservedException                          ;228
                DCD     OSReservedException                          ;229
                DCD     OSReservedException                          ;230
                DCD     OSReservedException                          ;231
                DCD     OSReservedException                          ;232
                DCD     OSReservedException                          ;233
                DCD     OSReservedException                          ;234
                DCD     OSReservedException                          ;235
                DCD     OSReservedException                          ;236
                DCD     OSReservedException                          ;237
                DCD     OSReservedException                          ;238
                DCD     OSReservedException                          ;239
                DCD     OSReservedException                          ;240
                DCD     OSReservedException                          ;241
                DCD     OSReservedException                          ;242
                DCD     OSReservedException                          ;243
                DCD     OSReservedException                          ;244
                DCD     OSReservedException                          ;245
                DCD     OSReservedException                          ;246
                DCD     OSReservedException                          ;247
                DCD     OSReservedException                          ;248
                DCD     OSReservedException                          ;249
                DCD     OSReservedException                          ;250
                DCD     OSReservedException                          ;251
                DCD     OSReservedException                          ;252
                DCD     OSReservedException                          ;253
                DCD     OSReservedException                          ;254
                DCD     0xFFFFFFFF                          ;Reserved for user TRIM value
__Vectors_End

__Vectors_Size 	EQU     __Vectors_End - __Vectors

; <h> Flash Configuration
;   <i> 16-byte flash configuration field that stores default protection settings (loaded on reset)
;   <i> and security information that allows the MCU to restrict access to the FTFL module.
;   <h> Backdoor Comparison Key
;     <o0>  Backdoor Comparison Key 0.  <0x0-0xFF:2>
;     <o1>  Backdoor Comparison Key 1.  <0x0-0xFF:2>
;     <o2>  Backdoor Comparison Key 2.  <0x0-0xFF:2>
;     <o3>  Backdoor Comparison Key 3.  <0x0-0xFF:2>
;     <o4>  Backdoor Comparison Key 4.  <0x0-0xFF:2>
;     <o5>  Backdoor Comparison Key 5.  <0x0-0xFF:2>
;     <o6>  Backdoor Comparison Key 6.  <0x0-0xFF:2>
;     <o7>  Backdoor Comparison Key 7.  <0x0-0xFF:2>
BackDoorK0      EQU     0xFF
BackDoorK1      EQU     0xFF
BackDoorK2      EQU     0xFF
BackDoorK3      EQU     0xFF
BackDoorK4      EQU     0xFF
BackDoorK5      EQU     0xFF
BackDoorK6      EQU     0xFF
BackDoorK7      EQU     0xFF
;   </h>
;   <h> Program flash protection bytes (FPROT)
;     <i> Each program flash region can be protected from program and erase operation by setting the associated PROT bit.
;     <i> Each bit protects a 1/32 region of the program flash memory.
;     <h> FPROT0
;       <i> Program Flash Region Protect Register 0
;       <i> 1/32 - 8/32 region
;       <o.0>   FPROT0.0
;       <o.1>   FPROT0.1
;       <o.2>   FPROT0.2
;       <o.3>   FPROT0.3
;       <o.4>   FPROT0.4
;       <o.5>   FPROT0.5
;       <o.6>   FPROT0.6
;       <o.7>   FPROT0.7
nFPROT0         EQU     0x00
FPROT0          EQU     nFPROT0:EOR:0xFF
;     </h>
;     <h> FPROT1
;       <i> Program Flash Region Protect Register 1
;       <i> 9/32 - 16/32 region
;       <o.0>   FPROT1.0
;       <o.1>   FPROT1.1
;       <o.2>   FPROT1.2
;       <o.3>   FPROT1.3
;       <o.4>   FPROT1.4
;       <o.5>   FPROT1.5
;       <o.6>   FPROT1.6
;       <o.7>   FPROT1.7
nFPROT1         EQU     0x00
FPROT1          EQU     nFPROT1:EOR:0xFF
;     </h>
;     <h> FPROT2
;       <i> Program Flash Region Protect Register 2
;       <i> 17/32 - 24/32 region
;       <o.0>   FPROT2.0
;       <o.1>   FPROT2.1
;       <o.2>   FPROT2.2
;       <o.3>   FPROT2.3
;       <o.4>   FPROT2.4
;       <o.5>   FPROT2.5
;       <o.6>   FPROT2.6
;       <o.7>   FPROT2.7
nFPROT2         EQU     0x00
FPROT2          EQU     nFPROT2:EOR:0xFF
;     </h>
;     <h> FPROT3
;       <i> Program Flash Region Protect Register 3
;       <i> 25/32 - 32/32 region
;       <o.0>   FPROT3.0
;       <o.1>   FPROT3.1
;       <o.2>   FPROT3.2
;       <o.3>   FPROT3.3
;       <o.4>   FPROT3.4
;       <o.5>   FPROT3.5
;       <o.6>   FPROT3.6
;       <o.7>   FPROT3.7
nFPROT3         EQU     0x00
FPROT3          EQU     nFPROT3:EOR:0xFF
;     </h>
;   </h>
;   <h> Data flash protection byte (FDPROT)
;     <i> Each bit protects a 1/8 region of the data flash memory.
;     <i> (Program flash only devices: Reserved)
;       <o.0>   FDPROT.0
;       <o.1>   FDPROT.1
;       <o.2>   FDPROT.2
;       <o.3>   FDPROT.3
;       <o.4>   FDPROT.4
;       <o.5>   FDPROT.5
;       <o.6>   FDPROT.6
;       <o.7>   FDPROT.7
nFDPROT         EQU     0x00
FDPROT          EQU     nFDPROT:EOR:0xFF
;   </h>
;   <h> EEPROM protection byte (FEPROT)
;     <i> FlexNVM devices: Each bit protects a 1/8 region of the EEPROM.
;     <i> (Program flash only devices: Reserved)
;       <o.0>   FEPROT.0
;       <o.1>   FEPROT.1
;       <o.2>   FEPROT.2
;       <o.3>   FEPROT.3
;       <o.4>   FEPROT.4
;       <o.5>   FEPROT.5
;       <o.6>   FEPROT.6
;       <o.7>   FEPROT.7
nFEPROT         EQU     0x00
FEPROT          EQU     nFEPROT:EOR:0xFF
;   </h>
;   <h> Flash nonvolatile option byte (FOPT)
;     <i> Allows the user to customize the operation of the MCU at boot time.
;     <o.0> LPBOOT
;       <0=> Core and system clock divider (OUTDIV1) is 0x1 (divide by 2).
;       <1=> Core and system clock divider (OUTDIV1) is 0x0 (divide by 1).
;     <o.2> NMI_DIS
;       <0=> NMI interrupts are always blocked
;       <1=> NMI_b pin/interrupts reset default to enabled
;     <o.3> RESET_PIN_CFG
;       <0=> RESET pin is disabled following a POR and cannot be enabled as reset function
;       <1=> RESET_b pin is dedicated
FOPT          EQU     0x7F
;   </h>
;   <h> Flash security byte (FSEC)
;     <i> WARNING: If SEC field is configured as "MCU security status is secure" and MEEN field is configured as "Mass erase is disabled",
;     <i> MCU's security status cannot be set back to unsecure state since Mass erase via the debugger is blocked !!!
;     <o.0..1> SEC
;       <2=> MCU security status is unsecure
;       <3=> MCU security status is secure
;         <i> Flash Security
;     <o.2..3> FSLACC
;       <2=> Freescale factory access denied
;       <3=> Freescale factory access granted
;         <i> Freescale Failure Analysis Access Code
;     <o.4..5> MEEN
;       <2=> Mass erase is disabled
;       <3=> Mass erase is enabled
;     <o.6..7> KEYEN
;       <2=> Backdoor key access enabled
;       <3=> Backdoor key access disabled
;         <i> Backdoor Key Security Enable
FSEC          EQU     0xFE
;   </h>
; </h>
                IF      :LNOT::DEF:RAM_TARGET
                AREA    FlashConfig, DATA, READONLY
__FlashConfig
                DCB     BackDoorK0, BackDoorK1, BackDoorK2, BackDoorK3
                DCB     BackDoorK4, BackDoorK5, BackDoorK6, BackDoorK7
                DCB     FPROT0    , FPROT1    , FPROT2    , FPROT3
                DCB     FSEC      , FOPT      , FEPROT    , FDPROT
                ENDIF


                AREA    |.text|, CODE, READONLY

; Reset Handler

Reset_Handler   PROC
                EXPORT  Reset_Handler             [WEAK]
                IMPORT  SystemInit
;               IMPORT  init_data_bss
                IMPORT  main

                IF      :LNOT::DEF:RAM_TARGET
                LDR R0, =FlashConfig    ; dummy read, workaround for flashConfig
                ENDIF

                CPSID   I               ; Mask interrupts
                LDR     R0, =SystemInit
                BLX     R0
;               LDR     R0, =init_data_bss
;               BLX     R0
                CPSIE   i               ; Unmask interrupts
                LDR     R0, =main
                BX      R0
                ENDP



                ALIGN


                END
