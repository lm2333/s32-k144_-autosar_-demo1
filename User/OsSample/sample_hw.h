#ifndef SAMPLEHW_H
#define SAMPLEHW_H
/**************************************************************************************
*
*   NXP(TM) and the NXP logo are trademarks of NXP
*   All other product or service names are the property of their respective owners.
*   (c) Copyright 2014 - 2016 Freescale Semiconductor Inc.
*   Copyright 2017 NXP
*   All Rights Reserved.
*
*   You can use this example for any purpose on any computer system with the
*   following restrictions:
*
*   1. This example is provided "as is", without warranty.
*
*   2. You don't remove this copyright notice from this example or any direct derivation
*      thereof.
*
*  Description:  AUTOSAR OS sample application
*
**************************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#include "Dio.h"

#define SetGPIO(n)      {Dio_WriteChannel(DioConf_DioChannel_DioChannel_LED_GREEN, STD_LOW);}
#define ClrGPIO(n)      {Dio_WriteChannel(DioConf_DioChannel_DioChannel_LED_GREEN, STD_HIGH);}
#define ToogleGPIO(n)   {if(n)Dio_FlipChannel(DioConf_DioChannel_DioChannel_LED_BLUE);\
                         else Dio_FlipChannel(DioConf_DioChannel_DioChannel_LED_GREEN);}

#define InitGPIO(n)

#define EnableAllPeriph()

#ifdef __cplusplus
}
#endif

#endif  /* SAMPLEHW_H */
