/**************************************************************************************
*
*   NXP(TM) and the NXP logo are trademarks of NXP
*   All other product or service names are the property of their respective owners.
*   (c) Copyright 2012 - 2016 Freescale Semiconductor Inc.
*   Copyright 2017 NXP
*   All Rights Reserved.
*
*   You can use this example for any purpose on any computer system with the
*   following restrictions:
*
*   1. This example is provided "as is", without warranty.
*
*   2. You don't remove this copyright notice from this example or any direct derivation
*      thereof.
*
*  Description:  AUTOSAR OS sample application
*
**************************************************************************************/

#include <Os.h>                              /* OS API */

#include <Ioc.h>

/* Global 'receive' application data */
#define RCV_APP_START_SEC_VAR_FAST_16
#include "MemMap.h"
#if defined(OSTSKFPU)
APP_NEAR_16 volatile unsigned short repeatCnt;
#else
APP_NEAR_16 volatile unsigned short taskRcv1, taskRcv2, repeatCnt;
#endif
#define RCV_APP_STOP_SEC_VAR_FAST_16
#include "MemMap.h"

#if defined(OSTSKFPU)
#define RCV_APP_START_SEC_VAR_FAST_32
#include "MemMap.h"
APP_NEAR_32 volatile float taskRcv1, taskRcv2;
#define RCV_APP_STOP_SEC_VAR_FAST_32
#include "MemMap.h"
#endif

/* Global 'trusted' application data */
#define TRUSTED_APP_START_SEC_VAR_FAST_16
#include "MemMap.h"
APP_NEAR_16 volatile unsigned short taskStop;
#define TRUSTED_APP_STOP_SEC_VAR_FAST_16
#include "MemMap.h"

#define APP_START_SEC_CODE
#include "MemMap.h"

#include "Mcu.h"
#include "Port.h"
#include "Dio.h"
#include "Can.h"
#include "SchM_Can.h"
#include "Fls.h"

#include "Can_Debug.h"


/***************************************************************************
 * Function:    main
 *
 * Description: inits variables and starts OS
 *
 **************************************************************************/
int main( void )
{
#if defined(OSIARARM)
    extern void SystemInit(void);
    SystemInit();
#endif

    /* Initialize the Mcu driver */
    Mcu_Init(&McuModuleConfiguration_0);

    Mcu_InitClock(McuClockSettingConfig_0);
#if (MCU_NO_PLL == STD_OFF)
    while ( MCU_PLL_LOCKED != Mcu_GetPllStatus() )
    {
        /* Busy wait until the System PLL is locked */
    }

    Mcu_DistributePllClock();
#endif
    Mcu_SetMode(McuModeSettingConf_0);

    /* Initialize all pins using the Port driver */
    Port_Init(&PortConfigSet_0);
    Fls_Init(&FlsConfigSet_0);

    /* Initilize Can driver */
    Can_Init(&CanConfigSet_0);
    Can_SetControllerMode(0, CAN_T_START);
    Can_SetControllerMode(1, CAN_T_START);
    Can_CreateDubugMsg();

    Dio_WriteChannel(DioConf_DioChannel_DioChannel_LED_GREEN, STD_HIGH);
    Dio_WriteChannel(DioConf_DioChannel_DioChannel_LED_BLUE, STD_HIGH);

    ind = 0;
    repeatCnt = 0;
    repeatCnt1 = 0;
#if defined(OSTSKFPU)
    taskRcv1 = 0.5;
    taskRcv2 = 0.5;
#else
    taskRcv1 = 0;
    taskRcv2 = 0;
#endif
    taskStop = 0;
#if defined(OSTSKFPU)
    taskSnd1 = 0.5;
    taskSnd2 = 0.5;
#else
    taskSnd1 = 0;
    taskSnd2 = 0;
#endif
    taskCnt  = 0;


    StartOS( Mode01 );                            /* jump to OS startup */

    return 0;
}


///***************************************************************************
// * Function:    OsIsr_0
// *
// * Description: User's ISR
// *
// * PROPERTIES:
// *
// **************************************************************************/
ISR( OsIsr_0 )
{
}
#define APP_STOP_SEC_CODE
#include "MemMap.h"

