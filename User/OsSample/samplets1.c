/**************************************************************************************
*
*   NXP(TM) and the NXP logo are trademarks of NXP
*   All other product or service names are the property of their respective owners.
*   (c) Copyright 2012 - 2016 Freescale Semiconductor Inc.
*   Copyright 2017 NXP
*   All Rights Reserved.
*
*   You can use this example for any purpose on any computer system with the
*   following restrictions:
*
*   1. This example is provided "as is", without warranty.
*
*   2. You don't remove this copyright notice from this example or any direct derivation
*      thereof.
*
*  Description:  AUTOSAR OS sample application
*
**************************************************************************************/

#include    <Os.h>                              /* OS API */

#include    <Ioc.h>

#include "Can.h"

#include "Can_Debug.h"

#include "Fls.h"

/* Global 'send' application data */
#define SND_APP_START_SEC_VAR_FAST_16
#include "MemMap.h"
#if defined(OSTSKFPU)
APP_NEAR_16 volatile unsigned short ind, taskCnt, repeatCnt1;
#else
APP_NEAR_16 volatile unsigned short ind, taskSnd1, taskSnd2, taskCnt, repeatCnt1;
#endif
#define SND_APP_STOP_SEC_VAR_FAST_16
#include "MemMap.h"

#if defined(OSTSKFPU)
#define SND_APP_START_SEC_VAR_FAST_32
#include "MemMap.h"
APP_NEAR_32 volatile float taskSnd1, taskSnd2;
#define SND_APP_STOP_SEC_VAR_FAST_32
#include "MemMap.h"
#endif

#define TRUSTED_APP_START_SEC_VAR_FAST_16
#include "MemMap.h"
/* User's hooks: */
APP_NEAR_16 int volatile hookNmb;               /* to prevent hooks from linker optimization */
#define TRUSTED_APP_STOP_SEC_VAR_FAST_16
#include "MemMap.h"

#define SND_APP_START_SEC_VAR_FAST_32
#include "MemMap.h"
//APP_NEAR_32 static int msgAnum;
#define SND_APP_STOP_SEC_VAR_FAST_32
#include "MemMap.h"

#define SND_APP_START_SEC_VAR_FAST_UNSPECIFIED
#include "MemMap.h"
//APP_NEAR_U static MSGBTYPE Msg_B;               /* declare message MsgB */
#define SND_APP_STOP_SEC_VAR_FAST_UNSPECIFIED
#include "MemMap.h"

#define OSHOOK_START_SEC_CODE
#include "MemMap.h"

void    ErrorHook( StatusType Error )           /* Error handling routine */
{
    hookNmb = Error;                            /* to avoid compiler warning */
    hookNmb = 1;
}

void    PreTaskHook( void )                     /* Routine to call before entering task context */
{
    hookNmb = 2;
}

void    PostTaskHook( void )                    /* Routine to call after saving task context */
{
    hookNmb = 3;
}


void ErrorHook_SND_APP(StatusType error)        /* SND application error hook */
{
    hookNmb = 4;
    while(1);
}

void ErrorHook_RCV_APP(StatusType error)        /* RCV application error hook */
{
    hookNmb = 5;
    while(1);
}

void ErrorHook_TRUSTED_APP(StatusType error)    /* TRUSTED application error hook */
{
    hookNmb = 6;
    while(1);
}



#define OSHOOK_STOP_SEC_CODE
#include "MemMap.h"

#define APP_START_SEC_CODE
#include "MemMap.h"

/* User's functions: */

/***************************************************************************
 * Function:    TASKSND1
 *
 * Description: Task sender - is activated by Time Scale
 *
 * PROPERTIES:  priority = 3, Basic, preemptive
 *
 **************************************************************************/

TASK( TASKLED )
{
    volatile StatusType status;                 /* variable to check system status */
    static uint8 i;

    if(++i >= 5)
    {
        i = 0;
        ToogleGPIO(0);
        Can_Write(CanConf_CanHardwareObject_HTH_0_0, &Can_PduInfo_0_0);
    }

    status = TerminateTask( );
}

TASK( OsTask_5ms )
{
    volatile StatusType status;                 /* variable to check system status */
    Can_MainFunction_Write();
    Can_MainFunction_Read();
    Fls_MainFunction();
    status = TerminateTask( );
}

#define APP_STOP_SEC_CODE
#include "MemMap.h"
