/*
*   (c) Copyright 2021 NXP
*
*   NXP Confidential. This software is owned or controlled by NXP and may only be used strictly
*   in accordance with the applicable license terms.  By expressly accepting
*   such terms or by downloading, installing, activating and/or otherwise using
*   the software, you are agreeing that you have read, and that you agree to
*   comply with and are bound by, such license terms.  If you do not agree to
*   be bound by the applicable license terms, then you may not retain,
*   install, activate or otherwise use the software.
*
*   This file contains sample code only. It is not part of the production code deliverables.
*/

#ifdef __cplusplus
extern "C" {
#endif


/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu.h"
#include "Port.h"
#include "Dio.h"
#include "Can.h"
#include "Fls.h"
#include "Fee.h"
#include "CanIf.h"
#include "Com.h"
#include "PduR.h"

#include "Can_Debug.h"
#include "FeeTest.h"

/*==================================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL FUNCTIONS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

void TestDelay(uint32 delay);
void TestDelay(uint32 delay)
{
   static volatile uint32 DelayTimer = 0;
   while(DelayTimer<delay)
   {
       DelayTimer++;
   }
   DelayTimer=0;
}
uint32 cnt;
//uint32 cnt2;

/**
* @brief        Main function of the example
* @details      Initializez the used drivers and uses the Icu
*               and Dio drivers to toggle a LED on a push button
*/
int main(void)
{
    /* Initialize the Mcu driver */
    Mcu_Init(&McuModuleConfiguration_0);

    Mcu_InitClock(McuClockSettingConfig_0);
#if (MCU_NO_PLL == STD_OFF)
    while ( MCU_PLL_LOCKED != Mcu_GetPllStatus() )
    {
        /* Busy wait until the System PLL is locked */
    }

    Mcu_DistributePllClock();
#endif
    Mcu_SetMode(McuModeSettingConf_RUN);

    /* Initialize all pins using the Port driver */
    Port_Init(&PortConfigSet_0);
    Fls_Init(&FlsConfigSet_0);
    Fee_Init();
    FeeState = FEE_ERASE_STATE;
    FeeBankIdx = 1;

    /* Initilize Can driver */
    Can_Init(&CanConfigSet_0);
//    Can_SetControllerMode(0, CAN_T_START);
//    Can_SetControllerMode(1, CAN_T_START);
    Can_CreateDubugMsg();
//    Can_Init(&CanConfigSet_0);
//    Can_SetControllerMode(0, CAN_T_START);
//    Can_SetControllerMode(1, CAN_T_START);
//    Can_CreateDubugMsg();
//
    CanIf_Init(&CanIf_Config);
    CanIf_SetControllerMode(CANIF_Controller_A, CANIF_CS_STARTED);
    CanIf_SetControllerMode(CANIF_Controller_B, CANIF_CS_STARTED);

    Com_Init(&ComConfiguration);
    PduRState = PDUR_UNINIT;
    PduR_Init (&PduR_Config);
    Com_IpduGroupStart(CanDbImportedPdus, TRUE);



    Dio_WriteChannel(DioConf_DioChannel_DioChannel_LED_GREEN, STD_HIGH);
    Dio_WriteChannel(DioConf_DioChannel_DioChannel_LED_BLUE, STD_HIGH);
//    memset(&Can_PduInfo_0_0.sdu[0], 0, 8);
//    Can_PduInfo_0_0.id = 0x200;
//    Can_Write(CanConf_CanHardwareObject_HTH_0_0, &Can_PduInfo_0_0);
    cnt = 1;

    StartOS( Mode01 );                            /* jump to OS startup */

    return 0;
}


TASK( TASKLED )
{
    volatile StatusType status;                 /* variable to check system status */

//    ToogleGPIO(0);
//    Can_Write(CanConf_CanHardwareObject_HTH_0_0, &Can_PduInfo_0_0);
//    Can_Write(CanConf_CanHardwareObject_HTH_1_0, &Can_PduInfo_1_0);

    cnt++;
//    if((cnt%10) == 0)
//    {
//        Com_SendSignal(CanDB_Signal_0_8_LE, &cnt);
//    }

    Com_SendSignal(CanDB_Signal_0_8_LE, &cnt);

    status = TerminateTask( );
}
TASK( OsTask_5ms )
{
    volatile StatusType status;                 /* variable to check system status */
    Can_MainFunction_Write();
    Can_MainFunction_Read();
    Can_MainFunction_BusOff();
    Fls_MainFunction();
    Fee_MainFunction();
    Com_MainFunctionRx();
    Com_MainFunctionTx();
    status = TerminateTask();
}

#ifdef __cplusplus
}
#endif

/** @} */
