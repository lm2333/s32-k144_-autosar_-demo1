#include "Can.h"

#include <Can_Debug.h>

uint8 Can_au8Sdu8bytes00[8U] = {0x01U, 0x02U, 0x03U, 0x04U, 0x05U, 0x06U, 0x07U, 0x08};
uint8 Can_au8Sdu8bytes01[8U] = {0x01U, 0x02U, 0x03U, 0x04U, 0x05U, 0x06U, 0x07U, 0x08};
uint8 Can_au8Sdu8bytes10[8U] = {0x01U, 0x02U, 0x03U, 0x04U, 0x05U, 0x06U, 0x07U, 0x08};
uint8 Can_au8Sdu8bytes11[8U] = {0x01U, 0x02U, 0x03U, 0x04U, 0x05U, 0x06U, 0x07U, 0x08};
Can_PduType Can_PduInfo_0_0, Can_PduInfo_0_1, Can_PduInfo_1_0, Can_PduInfo_1_1;

Can_PduType Can_CreatePduInfo(Can_IdType id, PduIdType swPduHandle, uint8 length, uint8* sdu)
{
    Can_PduType PduInfo;

    PduInfo.id = id;
    PduInfo.swPduHandle = swPduHandle;
    PduInfo.length = length;
    PduInfo.sdu = sdu;

    return PduInfo;
}



void Can_CreateDubugMsg()
{
    /* Can_CreatePduInfo(id, swPduHandle,length, sdu) */
    Can_PduInfo_0_0 = Can_CreatePduInfo(0x001, 0U, 8U, Can_au8Sdu8bytes00);
    Can_PduInfo_0_1 = Can_CreatePduInfo(0x002, 0U, 8U, Can_au8Sdu8bytes01);
    Can_PduInfo_1_0 = Can_CreatePduInfo(0x003, 0U, 8U, Can_au8Sdu8bytes10);
    Can_PduInfo_1_1 = Can_CreatePduInfo(0x004, 0U, 8U, Can_au8Sdu8bytes11);

}

