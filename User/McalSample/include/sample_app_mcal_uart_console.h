/**
    @file    sample_app_mcal_uart_console.h
    @version 1.0.4

    @brief   AUTOSAR Sample_app - header file for the initialization task for platform reg file.
    @details .

    Project      : AUTOSAR 4.0 MCAL
    Platform     : ARM

    ARVersion     : 4.0.3
    ARRevision    : ASR_REL_4_0_REV_0003
    ARConfVariant :
    SWVersion     : 1.0.4
    BuildVersion  : S32K14X_MCAL_1_0_4_RTM_ASR_REL_4_0_REV_0003_20190307

    (c) Copyright 2019 NXP
    All Rights Reserved.

    This file contains sample code only. It is not part of the production code deliverables.

*/
/*==================================================================================================
==================================================================================================*/

#ifndef SAMPLE_APP_MCAL_UART_CONSOLE_H
#define SAMPLE_APP_MCAL_UART_CONSOLE_H

#ifdef __cplusplus
extern "C" {
#endif

/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
/**
@file        modules.h
@brief Include Standard types & defines
*/

#include "typedefs.h"
#include "Std_Types.h"
#include "sample_app_mcal_data_definition.h"
#include <stdarg.h>

/*==================================================================================================
                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/

/**
@{
@brief Parameters that shall be published within the modules header file.
       The integration of incompatible files shall be avoided.
@remarks Covers
@remarks Implements
*/

/*==================================================================================================
                                      FILE VERSION CHECKS
==================================================================================================*/

/*==================================================================================================
                                           CONSTANTS
==================================================================================================*/

/*==================================================================================================
                                       DEFINES AND MACROS
==================================================================================================*/

/*==================================================================================================
                                             ENUMS
==================================================================================================*/

/*==================================================================================================
                                 STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
                                 GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
                                     FUNCTION PROTOTYPES
==================================================================================================*/


/* Console support specific functions */
void SampleApp_UART_ConsoleInit(void);
char SampleApp_GetChar(void);
void SampleApp_PutChar(char ch);
int  SampleApp_GetChar_present(void);
void SampleApp_WaitTx(void);

int SampleApp_Printf (char *text, uint32 value);

#define CONSOLE_MESSAGE SampleApp_Printf
#define CONSOLE_FLUSH SampleApp_WaitTx


#ifdef __cplusplus
}
#endif

#endif /* SAMPLE_APP_MCAL_UART_CONSOLE_H */

