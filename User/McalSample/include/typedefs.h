/**
    @file    typedefs.h
    @version 1.0.4

    @brief   AUTOSAR Sample_app - typedefs for platform reg file.
    @details .
    
    Project      : AUTOSAR 4.0 MCAL
    Platform     : ARM
    
    ARVersion     : 4.0.3
    ARRevision    : ASR_REL_4_0_REV_0003
    ARConfVariant :
    SWVersion     : 1.0.4
    BuildVersion  : S32K14X_MCAL_1_0_4_RTM_ASR_REL_4_0_REV_0003_20190307

    (c) Copyright 2019 NXP
    All Rights Reserved.

    This file contains sample code only. It is not part of the production code deliverables.
*/
/*==================================================================================================
==================================================================================================*/

#ifndef _TYPEDEFS_H_
#define _TYPEDEFS_H_

    

    typedef unsigned char uint8_t;
    typedef unsigned short uint16_t;
    typedef unsigned long uint32_t;
    typedef unsigned long long uint64_t;
    
    typedef signed char         int8_t;
    typedef signed short        int16_t;
    typedef signed long         int32_t;
    typedef signed long long    int64_t;

    /* Standard typedefs used by header files, based on ISO C standard */
    typedef volatile int8_t vint8_t;
    typedef volatile uint8_t vuint8_t;

    typedef volatile int16_t vint16_t;
    typedef volatile uint16_t vuint16_t;

    typedef volatile int32_t vint32_t;
    typedef volatile uint32_t vuint32_t;
    
    typedef volatile int64_t vint64_t;
    typedef volatile uint64_t vuint64_t;

#endif





