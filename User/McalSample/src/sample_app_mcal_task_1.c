/**
    @file        sample_app_mcal_task_1.c
    @version     1.0.4

    @brief       AUTOSAR - Autosar  Sample Application.
    @details     Sample application using AutoSar MCAL drivers.

    Project      : AUTOSAR 4.0 MCAL
    Platform     : ARM

    Autosar Version       : 4.0.3
    Autosar Revision      : ASR_REL_4_0_REV_0003
    Autosar Conf. Variant :
    Software Version      : 1.0.4
    Build Version         : S32K14X_MCAL_1_0_4_RTM_ASR_REL_4_0_REV_0003_20190307

    (c) Copyright 2019 NXP
    All Rights Reserved.

    This file contains sample code only. It is not part of the production code deliverables.

*/
/*==================================================================================================
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/

#include "sample_app_mcal_task_1.h"

/*==================================================================================================
                                        LOCAL MACROS
==================================================================================================*/
#define SAMPLE_APP_SECS_IN_MIN      60U
#define SAMPLE_APP_NOT_STARTED      0U
#define SAMPLE_APP_STARTED          1U
#define SAMPLE_APP_INACTIVE         0U
#define SAMPLE_APP_ACTIVE           1U

/*==================================================================================================
                                      FILE VERSION CHECKS
==================================================================================================*/


/*==================================================================================================
                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
                                       LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
                                       LOCAL VARIABLES
==================================================================================================*/

static volatile VAR(Dio_LevelType, AUTOMATIC) SampleApp_LedState = STD_HIGH;
static volatile VAR(uint8, AUTOMATIC) SampleApp_WdgState = SAMPLE_APP_INACTIVE;
static volatile VAR(uint8, AUTOMATIC) SampleApp_WdgStart = SAMPLE_APP_NOT_STARTED;
static volatile VAR(uint8, AUTOMATIC) SampleApp_GptStart = SAMPLE_APP_NOT_STARTED;
static volatile VAR(uint8, AUTOMATIC) SampleApp_BlinkOnce = TRUE;
static volatile VAR(uint32, AUTOMATIC) SampleApp_LifeTime = 0U;
/*==================================================================================================
                                       GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
                                       GLOBAL VARIABLES
==================================================================================================*/


/*==================================================================================================
                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/


/*==================================================================================================
                                       LOCAL FUNCTIONS
==================================================================================================*/


/*==================================================================================================
                                       GLOBAL FUNCTIONS
==================================================================================================*/

/**
@brief      SampleAppGptLed - Notification function called from Gpt driver
@details    This notification function is called by Gpt driver after the timer expire.

@return     none
@retval     none

@pre None
@post None
*/
#if (USE_GPT_MODULE == STD_ON)
FUNC (void, SAMPLE_APP_CODE) SampleAppGptLed(void)
{
#if (USE_DIO_MODULE == STD_ON)
    if (STD_HIGH == SampleApp_LedState)
    {
        SampleApp_LedState = STD_LOW;
    }
    else
    {
        SampleApp_LedState = STD_HIGH;
    }

    Dio_WriteChannel((Dio_ChannelType)DioChannelLed3, (Dio_LevelType)SampleApp_LedState);
   
    if (SampleApp_BlinkOnce == TRUE)
    {
        CONSOLE_MESSAGE("DIO Test Pass", 0);
        CONSOLE_MESSAGE("GPT Test Pass", 0);
   
		SampleApp_BlinkOnce = FALSE;
    }   
    
	++SampleApp_LifeTime;
   
    if ((0 == (SampleApp_LifeTime % SAMPLE_APP_SECS_IN_MIN)) && (SampleApp_LifeTime >= SAMPLE_APP_SECS_IN_MIN))
    {
        CONSOLE_MESSAGE("SampleApp run-time in minutes: ", (uint32)(SampleApp_LifeTime/SAMPLE_APP_SECS_IN_MIN));
    }  

#endif  /* #if (USE_DIO_MODULE == STD_ON) */
}
#endif  /* (USE_GPT_MODULE == STD_ON) */


/**
@brief      WdgExpire_Callback - Callback Function called from Wdg_Isr
@details    This notification function is called by Wdg driver after the timer expires.

@return     none
@retval     none

@pre None
@post None
*/
#if (USE_WDG_MODULE == STD_ON)
FUNC (void, SAMPLE_APP_CODE) WdgExpire_Callback (void)
{
    /* Turn OFF the Led2 */
    Dio_WriteChannel((Dio_ChannelType)DioChannelLed2, (Dio_LevelType)STD_HIGH);

	Dem_SetEventStatus(0x0U, 0x0U);
    /* Restart Wdg */
    CONSOLE_MESSAGE("WatchDog EVENT!!!", 0);
    /* move to normal operation state*/
    SampleApp_WdgState=SAMPLE_APP_INACTIVE;

#if (WDG_USE_INSTANCE_0  == STD_ON)
    Wdg_43_Instance0_SetMode(WDGIF_SLOW_MODE);
#else
    Wdg_SetMode(WDGIF_SLOW_MODE);
#endif	

}
#endif /*USE_WDG_MODULE == STD_ON)*/


/*================================================================================================*/
/**
@brief      SampleAppTask1 - Task1 function
@details    This function is called periodically by the OS or by the main() function.

@return     Returns the value of success
@retval     E_OK or E_NOT_OK

@pre None
@post None
*/
FUNC (Std_ReturnType, SAMPLE_APP_CODE) SampleAppTask1(P2VAR(SampleAppData_T, AUTOMATIC, SAMPLE_APP_VAR) pstSampleAppData)
{
    /* local variables here */
    Std_ReturnType stdRet = E_OK;

#if ((USE_WDG_MODULE == STD_ON) && (USE_DIO_MODULE == STD_ON))
    VAR(Dio_LevelType, AUTOMATIC) level;

    /* Read the status of the button */
    level = Dio_ReadChannel((Dio_ChannelType)DioChannelKey);

	/* Check if Button pressed */
	/* LOW - LED ON */
	/* HIGH - LED OFF */
	if (SAMPLE_APP_STARTED == SampleApp_WdgStart)
	{
		if ((SAMPLEAPP_BUTT_ON == level) && (SAMPLE_APP_INACTIVE == SampleApp_WdgState))
		{
			/* Button pressed */
			CONSOLE_MESSAGE("\nGoing to reset the LED NOW\n", 0);
			SampleApp_WdgState = SAMPLE_APP_ACTIVE;
			Dio_WriteChannel((Dio_ChannelType)DioChannelLed2, (Dio_LevelType)STD_LOW);
#if (WDG_USE_INSTANCE_0  == STD_ON)
			Wdg_43_Instance0_SetTriggerCondition(0);
#else
			Wdg_SetTriggerCondition(0);
#endif		
		}

		if (SAMPLE_APP_INACTIVE == SampleApp_WdgState)
		{
			/* Button was not pressed since last Wdg event - Normal Operation mode */
			/* Keep Led2 OFF */
			Dio_WriteChannel((Dio_ChannelType)DioChannelLed2, (Dio_LevelType)STD_HIGH);
			/* Trigger the Watchdog */
#if (WDG_USE_INSTANCE_0  == STD_ON)
			Wdg_43_Instance0_SetTriggerCondition(WDG_TRIGGER_VALUE);
#else
			Wdg_SetTriggerCondition(WDG_TRIGGER_VALUE);
#endif	
		}
	}
#endif  /* ((USE_WDG_MODULE == STD_ON) && (USE_DIO_MODULE == STD_ON)) */

#if (USE_GPT_MODULE == STD_ON)
    if(SAMPLE_APP_NOT_STARTED == SampleApp_GptStart)
    {
        Gpt_EnableNotification(0);
        Gpt_StartTimer(0, GPT_TIMER_LED_TICKS);
        SampleApp_GptStart = SAMPLE_APP_STARTED;
    }    
#endif  /* (USE_GPT_MODULE == STD_ON) */


#if (SAMPLE_APP_USE_FEE==STD_ON)
    #if ((USE_FEE_MODULE == STD_ON) && (USE_FLS_MODULE == STD_ON))

    if (FEE_FINAL_STATE != stSampleAppData.stFeeData.ucState)
    {
        /* start Watchdog and the Gpt timer after the Fee driver finished the execution */
        if (FEE_VALIDATE_STATE == stSampleAppData.stFeeData.ucState)
        {
            
       #if (USE_WDG_MODULE == STD_ON)
            Gpt_EnableNotification(1);
        
            #if (WDG_USE_INSTANCE_0  == STD_ON)
            Wdg_43_Instance0_SetMode(WDGIF_SLOW_MODE);
            #else
            Wdg_SetMode(WDGIF_SLOW_MODE);
            #endif    
            
            SampleApp_WdgStart = SAMPLE_APP_STARTED;
        #endif  /* (USE_WDG_MODULE == STD_ON) */    
        }
    
        /* execute Fee task */
        stdRet = SampleAppFeeTask(pstSampleAppData);
    }
    #endif /* ((USE_FEE_MODULE == STD_ON) && (USE_FLS_MODULE == STD_ON)) */
#else /*(SAMPLE_APP_USE_FEE==STD_OFF)*/

    #if (USE_WDG_MODULE==STD_ON)
    if (SAMPLE_APP_NOT_STARTED == SampleApp_WdgStart)
    {
        Gpt_EnableNotification(1);
         
        #if (WDG_USE_INSTANCE_0  == STD_ON)      
        Wdg_43_Instance0_SetMode(WDGIF_SLOW_MODE);
        #else
        Wdg_SetMode(WDGIF_SLOW_MODE);
        #endif
        
        SampleApp_WdgStart = SAMPLE_APP_STARTED;
    }
    #endif /* (USE_WDG_MODULE==STD_ON) */

#endif  /* (SAMPLE_APP_USE_FEE==STD_ON) */

    return (stdRet);
}
/*================================================================================================*/

#ifdef __cplusplus
}
#endif
