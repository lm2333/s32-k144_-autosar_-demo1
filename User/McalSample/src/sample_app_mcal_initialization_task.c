/**
    @file        sample_app_mcal_initialization_task.c
    @version     1.0.4

    @brief       AUTOSAR - Autosar  Sample Application.
    @details     Sample application using AutoSar MCAL drivers.

    Project      : AUTOSAR 4.0 MCAL
    Platform     : ARM

    Autosar Version       : 4.0.3
    Autosar Revision      : ASR_REL_4_0_REV_0003
    Autosar Conf. Variant :
    Software Version      : 1.0.4
    Build Version         : S32K14X_MCAL_1_0_4_RTM_ASR_REL_4_0_REV_0003_20190307

    (c) Copyright 2019 NXP
    All Rights Reserved.

    This file contains sample code only. It is not part of the production code deliverables.

*/
/*==================================================================================================
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
#include "sample_app_mcal_initialization_task.h"
#include "sys_init.h"

/*==================================================================================================
                                        LOCAL MACROS
==================================================================================================*/
#define NVIC_BASEADDR 0xE000E100
#define NVIC_ISER_OFFSET(id) (uint8)((id >> 5) << 2)
#define NVIC_ICER_OFFSET(id) (0x80 + (uint8)((id >> 5) << 2))
#define NVIC_IPRO_OFFSET(id) (0x300 + (uint8)((id >> 2) << 2))
#define NVIC_IPRO_MASK(id) (uint32)(0x000000FF << ((id - (id & 0xFCU)) << 3))

/*==================================================================================================
                                      FILE VERSION CHECKS
==================================================================================================*/


/*==================================================================================================
                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
                                       LOCAL CONSTANTS
==================================================================================================*/
#define SIUL_PCR_OBE_MASK           (uint16)0x0200
#define SIUL_PCR_IBE_MASK           (uint16)0x0100

/*==================================================================================================
                                       LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
                                       GLOBAL CONSTANTS
==================================================================================================*/
#if defined(AUTOSAR_OS_NOT_USED)
extern const vuint32_t IntcVectorTableHw[];
#endif

#if (USE_WDG_MODULE==STD_ON)
#ifdef WDG_ISR0_USED
extern ISR(Wdg_Wdog0_Isr);
#endif
#endif
#if (USE_GPT_MODULE==STD_ON)
extern ISR(LPIT_0_CH_0_ISR);
extern ISR(LPIT_0_CH_1_ISR);
#endif
#if (USE_ADC_MODULE==STD_ON)
extern ISR(Adc_Adc12bsarv2_EndGroupConvUnit0);
#endif
#if (USE_PWM_MODULE==STD_ON)
extern ISR(FTM_2_CH_0_CH_1_ISR);
extern ISR(FTM_2_OVF_ISR);
#endif
#if (USE_ICU_MODULE==STD_ON)
extern ISR(FTM_0_CH_6_CH_7_ISR);
#endif
#if (USE_SPI_MODULE==STD_ON)
extern ISR(Spi_LPspi_IsrTDF_LPSPI_0);
/*extern ISR(Spi_LPspi_IsrWCF_LPSPI_1);*/
#endif
#if (USE_LIN_MODULE==STD_ON)
extern ISR(Lin_LPUART_Isr_LPUART_0);
#endif
extern ISR(LPUART_Isr_UART_TX);
/*==================================================================================================
                                       GLOBAL VARIABLES
==================================================================================================*/

/**
@brief This is the global variable that holds all the sample app specific data
*/

/*==================================================================================================
                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/
FUNC (void, SAMPLE_APP_CODE) SampleApp_Int_Init(void);

/**
@brief      SampleApp_Int_Init - Interrupt initialization
@details    This function is called at the initialization stage to setup interrupts priorities

@return     none
@retval     none

@pre None
@post None
*/
#if defined(AUTOSAR_OS_NOT_USED)
FUNC (void, SAMPLE_APP_CODE) SampleApp_Int_Init(void)
{
    /* Setup all the needed interrupts(and their priorities) */
    sys_disableAllInterrupts();

#if (USE_GPT_MODULE==STD_ON)
    /* Gpt interrupts*/
    /* GPT_LPIT_0_CH_0_ISR_USED */
    sys_enableIsrSource(48, 0x70);
    sys_registerIsrHandler(48,(uint32)&LPIT_0_CH_0_ISR);

    /* Gpt interrupts*/
    /* GPT_LPIT_0_CH_1_ISR_USED */
    sys_enableIsrSource(49, 0x70);
    sys_registerIsrHandler(49,(uint32)&LPIT_0_CH_1_ISR);
#endif

#if (USE_WDG_MODULE==STD_ON)
#ifdef WDG_ISR0_USED
    
    /* Gpt - Wdg interrupts*/
    /* Gpt_PIT_TIMER_1_ISR */

    /* Wdg interrupts*/
    sys_enableIsrSource(22, 0xF0);
    sys_registerIsrHandler(22,(uint32)&Wdg_Wdog0_Isr);
#endif
#endif

#if (USE_ADC_MODULE==STD_ON)
    /* Adc Interrupts */
    sys_enableIsrSource(39, 0xF0);
    sys_registerIsrHandler(39,(uint32)&Adc_Adc12bsarv2_EndGroupConvUnit0);
#endif

#if (USE_PWM_MODULE==STD_ON)
    /* Pwm Interrupts */
    sys_enableIsrSource(111, 0xF0);
    sys_registerIsrHandler(111,(uint32)&FTM_2_CH_0_CH_1_ISR);
	
	sys_enableIsrSource(116, 0xF0);
    sys_registerIsrHandler(116,(uint32)&FTM_2_OVF_ISR);
#endif

#if (USE_LIN_MODULE==STD_ON)
    /* LIN Interrupts */
    sys_enableIsrSource(31, 0xF0);
    sys_registerIsrHandler(31,(uint32)&Lin_LPUART_Isr_LPUART_0);
#endif

#if (USE_ICU_MODULE==STD_ON)
    /* Icu Interrupts */
    sys_enableIsrSource(102, 0x30);
    sys_registerIsrHandler(102,(uint32)&FTM_0_CH_6_CH_7_ISR);
#endif

#if (USE_SPI_MODULE==STD_ON)
    /* SPI Interrupts */
    sys_enableIsrSource(26, 0xF0);
    sys_registerIsrHandler(26,(uint32)&Spi_LPspi_IsrTDF_LPSPI_0);
    /*sys_enableIsrSource(27, 0xF0);
    sys_registerIsrHandler(27,(uint32)&Spi_LPspi_IsrWCF_LPSPI_1);*/
    
#endif


#if (USE_LIN_MODULE==STD_ON)
    /* LIN Interrupts */
    
#endif
    /* UART APP Interrupts */
    sys_registerIsrHandler(33,(uint32)&LPUART_Isr_UART_TX);
    sys_enableIsrSource(33, 0xF0);
	
	/*sys_registerIsrHandler(34,(uint32)&LPUART_Isr_UART_RX);
    sys_enableIsrSource(33, 0xF0);*/

    /* Enable external Interrupts */
    sys_enableAllInterrupts();
}
#endif

/**
@brief      SampleAppInitTask - Sample Application Initialization
@details    This function is called at the initialization stage to initialize all MCAL drivers

@return     Returns the value of success
@retval     E_OK or E_NOT_OK

@pre None
@post None
*/
FUNC (Std_ReturnType, SAMPLE_APP_CODE) SampleAppInitTask(P2VAR(SampleAppData_T, AUTOMATIC, SAMPLE_APP_VAR) pstSampleAppData)
{
    /* local variables here */
    Std_ReturnType stdRet = E_OK;

/******************************************************************************/
/* DEM                                                                        */
/******************************************************************************/
    Dem_ReportErrorStatus(0, 0);
    Dem_SetEventStatus(0, 0);

/******************************************************************************/
/* DET                                                                        */
/******************************************************************************/
    Det_Init();
    Det_ReportError(0, 0, 0, 0);
    Det_Start();

/******************************************************************************/
/* MCU                                                                        */
/******************************************************************************/
#if (USE_MCU_MODULE==STD_ON)
/* MCU init called in main function */
#endif

/******************************************************************************/
/* PORT                                                                       */
/******************************************************************************/
#if (USE_PORT_MODULE==STD_ON)
    Port_Init(&PortConfigSetName);
#endif

/******************************************************************************/
/* ECUM                                                                       */
/******************************************************************************/
    EcuM_SetWakeupEvent(0);
    EcuM_ValidateWakeupEvent(0);
    EcuM_CheckWakeup(0);

/******************************************************************************/
/* FEE                                                                        */
/******************************************************************************/
#if (USE_FEE_MODULE==STD_ON)
    stdRet = SampleAppFeeInit(pstSampleAppData);
#endif



/******************************************************************************/
/* ADC                                                                        */
/******************************************************************************/
#if (USE_ADC_MODULE==STD_ON)
    stdRet = SampleAppAdcInit(pstSampleAppData);
    if (stdRet != E_OK)
    {
        return (E_NOT_OK);
    }
#endif


/******************************************************************************/
/* CAN                                                                        */
/******************************************************************************/
#if (USE_CAN_MODULE==STD_ON)
    stdRet = SampleAppCanInit(pstSampleAppData);
#endif

/******************************************************************************/
/* DIO                                                                        */
/******************************************************************************/
#if (USE_DIO_MODULE==STD_ON)
#endif

/******************************************************************************/
/* FLS                                                                        */
/******************************************************************************/
#if (USE_FLS_MODULE==STD_ON)
#endif

/******************************************************************************/
/* FR                                                                        */
/******************************************************************************/
#if (USE_FR_MODULE==STD_ON)
    stdRet = SampleAppFrInit(pstSampleAppData);
#endif


/******************************************************************************/
/* LIN                                                                        */
/******************************************************************************/
#if (USE_LIN_MODULE==STD_ON)
    stdRet = SampleAppLinInit(pstSampleAppData);
#endif


/******************************************************************************/
/* ICU                                                                        */
/******************************************************************************/
#if (USE_ICU_MODULE==STD_ON)
    stdRet = SampleAppIcuInit(pstSampleAppData);
    if (stdRet != E_OK)
    {
        return (E_NOT_OK);
    }
#endif

/******************************************************************************/
/* SPI                                                                        */
/******************************************************************************/
#if (USE_SPI_MODULE==STD_ON)
    stdRet = SampleAppSpiInit(pstSampleAppData);
    if (stdRet != E_OK)
    {
        return (E_NOT_OK);
    }
#endif

/******************************************************************************/
/* PWM                                                                        */
/******************************************************************************/
#if (USE_PWM_MODULE==STD_ON)
    stdRet = SampleAppPwmInit(pstSampleAppData);
    if (stdRet != E_OK)
    {
        return (E_NOT_OK);
    }
#endif

#if defined(AUTOSAR_OS_NOT_USED)
/******************************************************************************/
/* Init Interrupts                                                            */
/******************************************************************************/
    SampleApp_Int_Init();
#endif


/******************************************************************************/
/* Init uart console                                                          */
/******************************************************************************/
    SampleApp_UART_ConsoleInit();
	
#if defined(AUTOSAR_OS_NOT_USED)	
	CONSOLE_MESSAGE("MCAL SAMPLE APPLICATION: OS not present", 0);
#else	
	CONSOLE_MESSAGE("MCAL SAMPLE APPLICATION: OS started", 0);
#endif

    return (E_OK);
}
/*================================================================================================*/

#ifdef __cplusplus
}
#endif
