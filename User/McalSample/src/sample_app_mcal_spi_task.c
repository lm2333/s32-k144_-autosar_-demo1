/**
    @file        sample_app_mcal_spi_task.c
    @version     1.0.4

    @brief       AUTOSAR - Autosar  Sample Application.
    @details     Sample application using AutoSar MCAL drivers.

    Project      : AUTOSAR 4.0 MCAL
    Platform     : ARM

    Autosar Version       : 4.0.3
    Autosar Revision      : ASR_REL_4_0_REV_0003
    Autosar Conf. Variant :
    Software Version      : 1.0.4
    Build Version         : S32K14X_MCAL_1_0_4_RTM_ASR_REL_4_0_REV_0003_20190307

    (c) Copyright 2019 NXP
    All Rights Reserved.

    This file contains sample code only. It is not part of the production code deliverables.

*/
/*==================================================================================================
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/

#include "sample_app_mcal_spi_task.h"

/*==================================================================================================
                                        LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
                                      FILE VERSION CHECKS
==================================================================================================*/


/*==================================================================================================
                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
                                       LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
                                       LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
                                       GLOBAL CONSTANTS
==================================================================================================*/
#if (USE_SPI_MODULE==STD_ON)
/**
@brief This is the global variable that holds all the sample app specific data
*/

#define SPI_CALLBACK_CALLED               0xFF


/*==================================================================================================
                                       GLOBAL VARIABLES
==================================================================================================*/
static const SampleApp_SpiDataType SampleApp_ucSource[SPI_DATA_LENGTH] = {0xA0, 0x0A, 0xAA, 0x05, 0x50, 0x55, 0xFF, 0x00};
static SampleApp_SpiDataType SampleApp_ucDest[SPI_DATA_LENGTH];


/*==================================================================================================
                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/


/*==================================================================================================
                                       LOCAL FUNCTIONS
==================================================================================================*/


/*==================================================================================================
                                       GLOBAL FUNCTIONS
==================================================================================================*/



/*================================================================================================*/
/**
@brief      Spi_Job0_EndNotification - Notification function called from Spi driver
@details    This notification function is called by Spi driver after job end.

@return     none
@retval     none

@pre None
@post None
*/
/* Job End Notifications */
void Spi_Job0_EndNotification(void) /* End Notification for Job 'SpiJob_0' */
{
}

/*================================================================================================*/
/**
@brief      Spi_Seq0_EndNotification - Notification function called from Spi driver
@details    This notification function is called by Spi driver after sequence end.

@return     none
@retval     none

@pre None
@post None
*/
/* Sequence End Notifications */
void Spi_Seq0_EndNotification(void) /* End Notification for Sequence 'SpiSequence_0' */
{
    stSampleAppData.stSpiData.bFlags = SPI_CALLBACK_CALLED;
}

/*================================================================================================*/
/**
@brief      SampleAppSpiInit - Initialize the Spi task
@details    After calling this function the task containing Spi driver should be initialized

@return     Returns the value of success
@retval     E_OK or E_NOT_OK

@pre None
@post None
*/
FUNC (Std_ReturnType, SAMPLE_APP_CODE) SampleAppSpiInit(P2VAR(SampleAppData_T, AUTOMATIC, SAMPLE_APP_VAR) pstSampleAppData)
{
    /* local variables here */
    Std_ReturnType stdRet;

    /* set the first state in the state-machine */
    pstSampleAppData->stSpiData.ucState = SPI_INITIAL_STATE;
    /*CONSOLE_MESSAGE("SPI state-machine state:%d", pstSampleAppData->stSpiData.ucState);*/

    /* setup the Spi PB configuration pointer */
    pstSampleAppData->stSpiData.pSpiDriverConfig = (Spi_ConfigType *)(&SpiDriverName);

    Spi_Init(pstSampleAppData->stSpiData.pSpiDriverConfig);

    
	stdRet = Spi_SetAsyncMode(SPI_INTERRUPT_MODE);
    if (stdRet != E_OK)
    {
        CONSOLE_MESSAGE("SPI Error %d returned by Spi_SetAsyncMode(SPI_INTERRUPT_MODE)", stdRet);
        /*CONSOLE_MESSAGE("SPI state-machine state:%d", pstSampleAppData->stSpiData.ucState);*/
        pstSampleAppData->stSpiData.ucState = SPI_ERROR_STATE;
        return (E_OK);
    }

    /* everything worked fine - set the state-machine and return E_OK */
    pstSampleAppData->stSpiData.ucState = SPI_INITIALIZED_STATE;
    /*CONSOLE_MESSAGE("SPI state-machine state:%d", pstSampleAppData->stSpiData.ucState);*/
    return(E_OK);
}
/*================================================================================================*/
/**
@brief      SampleAppSpiTask - Contains the Spi task
@details

@return     Returns the value of success of executing the Spi task
@retval     E_OK or E_NOT_OK

@pre None
@post None
*/
FUNC (Std_ReturnType, SAMPLE_APP_CODE) SampleAppSpiTask(P2VAR(SampleAppData_T, AUTOMATIC, SAMPLE_APP_VAR) pstSampleAppData)
{
    /* local variables here */
    Std_ReturnType stdRet;
    uint32 ulCounter;

    /* state-machine logic */
    switch(pstSampleAppData->stSpiData.ucState)
    {
        case SPI_INITIALIZED_STATE:
        {
            stdRet = Spi_WriteIB( SpiChannelName, SampleApp_ucSource);
            if (stdRet != E_OK)
            {
                CONSOLE_MESSAGE("SPI Error returned by Spi_WriteIB( SpiChannel_0, ucTemp)", stdRet);
                /*CONSOLE_MESSAGE("SPI state-machine state:%d", pstSampleAppData->stSpiData.ucState);*/
                pstSampleAppData->stSpiData.ucState = SPI_ERROR_STATE;
                return (E_OK);
            }

            /* change the state */
            pstSampleAppData->stSpiData.ucState = SPI_TRANSMIT_STATE;
            return (E_OK);
        }
        /*break;*/

        case SPI_TRANSMIT_STATE:
        {
            stdRet = Spi_AsyncTransmit(SpiSequenceName);
            if (stdRet != E_OK)
            {
                CONSOLE_MESSAGE("SPI Error returned by Spi_AsyncTransmit(SpiSequence_0)", stdRet);
                /*CONSOLE_MESSAGE("SPI state-machine state:%d", pstSampleAppData->stSpiData.ucState);*/
                pstSampleAppData->stSpiData.ucState = SPI_ERROR_STATE;
                return (E_OK);
            }

            /* setup the timer */
            pstSampleAppData->stSpiData.ulTimeout = SPI_TRANSMIT_TIMEOUT;

            /* reset the callback flag */
            pstSampleAppData->stSpiData.bFlags = 0;

            /* change the state */
            pstSampleAppData->stSpiData.ucState = SPI_TX_PENDING_STATE;
            return (E_OK);
        }
        /*break;*/

        case SPI_TX_PENDING_STATE:
        {
            stdRet =  Spi_GetSequenceResult(SpiSequenceName);
            if (stdRet == SPI_SEQ_PENDING)
            {
                if (pstSampleAppData->stSpiData.ulTimeout > 0)
                {
                    /* decrease the timer*/
                    pstSampleAppData->stSpiData.ulTimeout--;
                    return (E_OK);
                }
                else
                {
                    /* the sequence is still pending and we have timeout */
                    CONSOLE_MESSAGE("SPI Error transmission timeout", 0);
                    /*CONSOLE_MESSAGE("SPI state-machine state:%d", pstSampleAppData->stSpiData.ucState);*/
                    /* change the state */
                    pstSampleAppData->stSpiData.ucState = SPI_ERROR_STATE;
                    return (E_OK);
                }
            }

            /* check if the sequence has been transmitted ok */
            /* also check that the notification callback has been called */
            if ((stdRet == SPI_SEQ_OK) && (pstSampleAppData->stSpiData.bFlags == SPI_CALLBACK_CALLED))
            {
                stdRet = Spi_ReadIB(SpiChannelName, SampleApp_ucDest);
                if (stdRet != E_OK)
                {
                    CONSOLE_MESSAGE("SPI Error returned by Spi_ReadIB( SpiChannel_0, SampleApp_ucDest)", stdRet);
                    /*CONSOLE_MESSAGE("SPI state-machine state:%d", pstSampleAppData->stSpiData.ucState);*/
                    pstSampleAppData->stSpiData.ucState = SPI_ERROR_STATE;
                    return (E_OK);
                }

                /* now compare the data and validate them */
                for (ulCounter = 0; ulCounter < SPI_DATA_LENGTH; ulCounter++)
                {
                    if (SampleApp_ucSource[ulCounter] != SampleApp_ucDest[ulCounter])
                    {
                        CONSOLE_MESSAGE("SPI Error-Tx/Rx data does not match SampleApp_ucSource , SampleApp_ucDest", ulCounter);
                        /*CONSOLE_MESSAGE("SPI state-machine state:%d", pstSampleAppData->stSpiData.ucState);*/
                        pstSampleAppData->stSpiData.ucState = SPI_ERROR_STATE;
                        return (E_OK);
                    }
                }

                /* everything went fine if we are here */
                pstSampleAppData->stSpiData.ucState = SPI_FINAL_STATE;
                return (E_OK);
            }
            else
            {
                /* the sequence transmission is canceled */
                CONSOLE_MESSAGE("SPI Error transmission", 0);
                /* change the state */
                pstSampleAppData->stSpiData.ucState = SPI_ERROR_STATE;
                return (E_OK);
            }
        }
        /*break;*/

        case SPI_FINAL_STATE:
        {
            /* the task ended successfully */
            CONSOLE_MESSAGE("\n SPI task ended OK.\n", 0);
            pstSampleAppData->stSpiData.ucState = SPI_STALLED_STATE;
            /*CONSOLE_MESSAGE("SPI state-machine state:%d", pstSampleAppData->stSpiData.ucState);*/
            return (E_OK);
        }
        /*break;*/

        case SPI_INITIAL_STATE:
        {
            CONSOLE_MESSAGE("SPI Error not initialized", 0);
            pstSampleAppData->stSpiData.ucState = SPI_ERROR_STATE;
            return (E_OK);
        }
        /*break;*/

        case SPI_ERROR_STATE:
        {
            /* we ended up in error - do nothing */
            return (E_OK);
        }
        /*break;*/

        case SPI_STALLED_STATE:
        {
            /*pstSampleAppData->stSpiData.ucState = SPI_INITIALIZED_STATE;*/
            return (E_OK);
        }
        /*break;*/

        default:
        {
            /* if we got here, something went terribly wrong*/
        }
        break;
    }

    return (E_NOT_OK);

}
#endif
/*================================================================================================*/
#ifdef __cplusplus
}
#endif
