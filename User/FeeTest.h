
#include "Fls.h"
#include "Fee.h"
typedef enum
{
/* FEE state machine */
    FEE_INITIAL_STATE,
    FEE_ERROR_STATE,
    FEE_ERASE_STATE,
    FEE_ERASE_BUSY_STATE,
    FEE_WRITE_STATE,
    FEE_WRITE_BUSY_STATE,
    FEE_READ_STATE,
    FEE_READ_BUSY_STATE,
    FEE_READ_ERROR_STATE,
    FEE_VALIDATE_STATE,
    FEE_FINAL_STATE
} FeeDriverState;

extern FeeDriverState FeeState;      /* State machine state */
extern VAR( uint16, AUTOMATIC ) FeeBankIdx;

/* Fee upper layer Job End Notifications */
void Fee_JobEndNotif(void);
/* Fee upper layer Job Error Notifications */
void Fee_JobErrorNotif(void);

