
#include "Can.h"

extern uint8 Can_au8Sdu8bytes[8U];
extern Can_PduType Can_PduInfo_0_0;
extern Can_PduType Can_PduInfo_0_1;
extern Can_PduType Can_PduInfo_1_0;
extern Can_PduType Can_PduInfo_1_1;
#define HighByte(word)  ((uint8)((uint16)word >> 8))
#define LowByte(word)   ((uint8)word)
Can_PduType Can_CreatePduInfo(Can_IdType id, PduIdType swPduHandle, uint8 length, uint8* sdu);
void Can_CreateDubugMsg(void);


