/*
* Configuration of module: CanIf (CanIf_Cfg.h)
*
* Created by:
* Copyright:
*
* Configured for (MCU):    STM32_F107
*
* Module vendor:           ArcCore
* Generator version:       2.0.6
*
* Generated by Arctic Studio (http://arccore.com)
*/


#if !(((CANIF_SW_MAJOR_VERSION == 1) && (CANIF_SW_MINOR_VERSION == 3)) )
#error CanIf: Configuration file expected BSW module version to be 1.3.*
#endif


#ifndef CANIF_CFG_H_
#define CANIF_CFG_H_

#include "Can.h"


#define CANIF_VERSION_INFO_API              STD_ON
#define CANIF_DEV_ERROR_DETECT			    STD_OFF
#define CANIF_DLC_CHECK                     STD_ON
#define CANIF_ARC_RUNTIME_PDU_CONFIGURATION	STD_OFF
#define CANIF_MULITPLE_DRIVER_SUPPORT       STD_OFF  // Not supported
#define CANIF_READRXPDU_DATA_API			STD_OFF  // Not supported
#define CANIF_READRXPDU_NOTIFY_STATUS_API	STD_OFF  // Not supported
#define CANIF_READTXPDU_NOTIFY_STATUS_API	STD_OFF  // Not supported
#define CANIF_SETDYNAMICTXID_API            STD_OFF  // Not supported
#define CANIF_WAKEUP_EVENT_API			    STD_OFF  // Not supported
#define CANIF_TRANSCEIVER_API               STD_OFF  // Not supported
#define CANIF_TRANSMIT_CANCELLATION         STD_OFF  // Not supported


#define CANIF_PDU_ID_CanDB_Message_1		0
#define CANIF_PDU_ID_CanDB_Message_2		1
#define CANIF_PDU_ID_CanDB_Message_3		2
#define CANIF_PDU_ID_CanDB_Message_4		3
#define CANIF_PDU_ID_CanDB_Message_5		4
#define CANIF_PDU_ID_CanDB_Message_6		5
#define CANIF_PDU_ID_CanDB_Message_7		6
#define CANIF_PDU_ID_CanDB_Message_8		7

// Identifiers for the elements in CanIfControllerConfig[]
// This is the ConfigurationIndex in CanIf_InitController()
typedef enum {
	CANIF_Controller_A_CONFIG_0,
	CANIF_Controller_B_CONFIG_0,
	CANIF_CHANNEL_CONFIGURATION_CNT
} CanIf_Arc_ConfigurationIndexType;

typedef enum {
	CANIF_Controller_A,
	CANIF_Controller_B,
	CANIF_CHANNEL_CNT
} CanIf_Arc_ChannelIdType;

#define CANIF_CONTROLLER_ID_Controller_1	CANIF_Controller_A
#define CANIF_CONTROLLER_ID_Controller_2	CANIF_Controller_B


typedef enum {
	CAN_ARC_HANDLE_TYPE_BASIC,
	CAN_ARC_HANDLE_TYPE_FULL
} Can_Arc_HohType;

typedef enum {
	HWObj_4 = 4,
	HWObj_5,
	HWObj_6,
	HWObj_7,
	NUM_OF_HTHS
} Can_Arc_HTHType;


typedef enum {
	HWObj_0 = 0,
	HWObj_1,
	HWObj_2,
	HWObj_3,
	NUM_OF_HRHS
} Can_Arc_HRHType;

typedef enum {
    CAN_CTRL_1 = 0,
    CAN_CTRL_2 = 1,
    CAN_CONTROLLER_CNT = 2
}CanControllerIdType;


#include "CanIf_ConfigTypes.h"


extern const CanIf_ConfigType CanIf_Config;

#endif

