/**
*   @file    Spi_Lcfg.c
*   @implements Spi_Lcfg.c_Artifact
*   @version 1.0.4
*
*   @brief   AUTOSAR Spi - Link-Time(L) configuration file code template.
*   @details Code template for Link-Time(L) configuration file generation.
*
*   @addtogroup [SPI_MODULE]
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.0 MCAL
*   Platform             : ARM
*   Peripheral           : LPSPI,FLEXIO
*   Dependencies         : 
*
*   Autosar Version      : 4.0.3
*   Autosar Revision     : ASR_REL_4_0_REV_0003
*   Autosar Conf.Variant :
*   SW Version           : 1.0.4
*   Build Version        : S32K14xS32K14x_MCAL_1_0_4_RTM_ASR_REL_4_0_REV_0003_20190307
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2018-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/

#ifdef __cplusplus
extern "C"{
#endif

/**
* @page misra_violations MISRA-C:2004 violations
*
* @section [global]
* Violates MISRA 2004 Required Rule 5.1, Identifiers (internal and external) shall not rely
* on the significance of more than 31 characters.
*
* @section Spi_Lcfg_c_REF_1
* Violates MISRA 2004 Advisory Rule 19.1,
* #include statements in a file should only be preceded by other preprocessor directives or comments.
* AUTOSAR imposes the specification of the sections in which certain parts of the driver must be placed.
*
* @section Spi_Lcfg_c_REF_2
* Violates MISRA 2004 Required Rule 19.15,
* Precautions shall be taken in order to prevent the contents of a header file being included twice.
* This comes from the order of includes in the .c file and from include dependencies. As a safe
* approach, any file must include all its dependencies. Header files are already protected against
* double inclusions. The inclusion of MemMap.h is as per Autosar requirement MEMMAP003.
*
* @section Spi_Lcfg_c_REF_3
* Violates MISRA 2004 Required Rule 1.4,
* The compiler/linker shall be checked to ensure that 31 character signifiance and case sensitivity are
* supported for external identifiers.
* This violation is due to the requirement that requests to have a file version check.
*
* @section Spi_Lcfg_c_REF_4
* Violates MISRA 2004 Required Rule 8.10, All declarations and definitions of objects or functions
* at file scope shall
* have internal linkage unless external linkage is required.
* State variables may be used by LLD layer.
*
*/


/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/


#include "Spi.h"
#include "Reg_eSys_LPspi.h"
#include "Reg_eSys_FlexIOSPI.h"

#if (SPI_DISABLE_DEM_REPORT_ERROR_STATUS == STD_OFF)
#include "Dem.h"
#endif /* SPI_DISABLE_DEM_REPORT_ERROR_STATUS == STD_OFF */

#if (SPI_CONFIG_VARIANT == SPI_VARIANT_LINKTIME)

/*==================================================================================================
*                                      LOCAL MACROS
==================================================================================================*/

/*
* @violates @ref Spi_Lcfg_c_REF_3 The compiler/linker shall be checked to ensure that 31 character
* signifiance and case sensitivity are supported for external identifiers.
*/
#define SPI_MODULE_ID_LTCFG_C                            83
#define SPI_VENDOR_ID_LTCFG_C                            43
/*
* @violates @ref Spi_Lcfg_c_REF_3 The compiler/linker shall be checked to ensure that 31 character
* signifiance and case sensitivity are supported for external identifiers.
*/
#define SPI_AR_RELEASE_MAJOR_VERSION_LTCFG_C             4
/*
* @violates @ref Spi_Lcfg_c_REF_3 The compiler/linker shall be checked to ensure that 31 character
* signifiance and case sensitivity are supported for external identifiers.
*/
#define SPI_AR_RELEASE_MINOR_VERSION_LTCFG_C             0
/*
* @violates @ref Spi_Lcfg_c_REF_3 The compiler/linker shall be checked to ensure that 31 character
* signifiance and case sensitivity are supported for external identifiers.
*/
#define SPI_AR_RELEASE_REVISION_VERSION_LTCFG_C          3
#define SPI_SW_MAJOR_VERSION_LTCFG_C                     1
#define SPI_SW_MINOR_VERSION_LTCFG_C                     0
#define SPI_SW_PATCH_VERSION_LTCFG_C                     4

/*==================================================================================================
                                      FILE VERSION CHECKS
==================================================================================================*/


/* Check if current file and SPI header file are of the same vendor */
#if (SPI_VENDOR_ID_LTCFG_C != SPI_VENDOR_ID)
    #error "Spi_Lcfg.c and Spi.h have different vendor ids"
#endif
/* Check if current file and SPI header file are of the same Autosar version */
#if ((SPI_AR_RELEASE_MAJOR_VERSION_LTCFG_C    != SPI_AR_RELEASE_MAJOR_VERSION) || \
     (SPI_AR_RELEASE_MINOR_VERSION_LTCFG_C    != SPI_AR_RELEASE_MINOR_VERSION) || \
     (SPI_AR_RELEASE_REVISION_VERSION_LTCFG_C != SPI_AR_RELEASE_REVISION_VERSION))
    #error "AutoSar Version Numbers of Spi_Lcfg.c and Spi.h are different"
#endif
/* Check if current file and SPI header file are of the same Software version */
#if ((SPI_SW_MAJOR_VERSION_LTCFG_C != SPI_SW_MAJOR_VERSION) || \
     (SPI_SW_MINOR_VERSION_LTCFG_C != SPI_SW_MINOR_VERSION) || \
     (SPI_SW_PATCH_VERSION_LTCFG_C != SPI_SW_PATCH_VERSION))
    #error "Software Version Numbers of Spi_Lcfg.c and Spi.h are different"
#endif

/* Check if current file and Reg_eSys_LPspi.h file are of the same vendor */
#if (SPI_VENDOR_ID_LTCFG_C != REG_ESYS_LPSPI_VENDOR_ID_REGFLEX)
    #error "Spi_Lcfg.c and Reg_eSys_LPspi.h have different vendor ids"
#endif
/* Check if current file and Reg_eSys_LPspi.h file are of the same Autosar version */
#if ((SPI_AR_RELEASE_MAJOR_VERSION_LTCFG_C    != REG_ESYS_LPSPI_AR_RELEASE_MAJOR_VERSION_REGFLEX) || \
     (SPI_AR_RELEASE_MINOR_VERSION_LTCFG_C    != REG_ESYS_LPSPI_AR_RELEASE_MINOR_VERSION_REGFLEX) || \
     (SPI_AR_RELEASE_REVISION_VERSION_LTCFG_C != REG_ESYS_LPSPI_AR_RELEASE_REVISION_VERSION_REGFLEX))
    #error "AutoSar Version Numbers of Spi_Lcfg.c and Reg_eSys_LPspi.h are different"
#endif
/* Check if current file and Reg_eSys_LPspi.h file are of the same Software version */
#if ((SPI_SW_MAJOR_VERSION_LTCFG_C != REG_ESYS_LPSPI_SW_MAJOR_VERSION_REGFLEX) || \
     (SPI_SW_MINOR_VERSION_LTCFG_C != REG_ESYS_LPSPI_SW_MINOR_VERSION_REGFLEX) || \
     (SPI_SW_PATCH_VERSION_LTCFG_C != REG_ESYS_LPSPI_SW_PATCH_VERSION_REGFLEX))
    #error "Software Version Numbers of Spi_Lcfg.c and Reg_eSys_LPspi.h are different"
#endif
#ifndef DISABLE_MCAL_INTERMODULE_ASR_CHECK
#if (SPI_DISABLE_DEM_REPORT_ERROR_STATUS == STD_OFF)
/* Check if current file and Dem.h file are of the same Autosar version */
#if ((SPI_AR_RELEASE_MAJOR_VERSION_LTCFG_C    != DEM_AR_RELEASE_MAJOR_VERSION) || \
     (SPI_AR_RELEASE_MINOR_VERSION_LTCFG_C    != DEM_AR_RELEASE_MINOR_VERSION))
    #error "AutoSar Version Numbers of Spi_Lcfg.c and Dem.h are different"
#endif
#endif /* SPI_DISABLE_DEM_REPORT_ERROR_STATUS == STD_OFF */
#endif


/*==================================================================================================
*                         LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                  LOCAL FUNCTION PROTOTYPES
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL FUNCTIONS
==================================================================================================*/

/*==================================================================================================
*                                      GLOBAL FUNCTIONS
==================================================================================================*/
#define SPI_START_SEC_CODE
/*
* @violates @ref Spi_Lcfg_c_REF_1 #include statements in a file should only be preceded
*        by other preprocessor directives or comments.
* @violates @ref Spi_Lcfg_c_REF_2 Precautions shall be taken in order to prevent the contents
*        of a header file being included twice.
*/
#include "MemMap.h"



/*
* @violates @ref Spi_PBcfg_c_REF_8 An external object or function shall be declared in one and only one file.
*/
extern void Spi_Job0_EndNotification(void); /* End Notification for Job 'SpiJob_0' */
/*
* @violates @ref Spi_PBcfg_c_REF_8 An external object or function shall be declared in one and only one file.
*/
extern void Spi_Seq0_EndNotification(void); /* End Notification for Sequence 'SpiSequence_0' */


#define SPI_STOP_SEC_CODE
/*
* @violates @ref Spi_Lcfg_c_REF_1 #include statements in a file should only be preceded
*        by other preprocessor directives or comments.
* @violates @ref Spi_Lcfg_c_REF_2 Precautions shall be taken in order to prevent the contents
*        of a header file being included twice.
*/
#include "MemMap.h"

/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/

/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/

#if ((SPI_DMA_USED == STD_ON) && \
    ((SPI_LEVEL_DELIVERED == LEVEL1) || (SPI_LEVEL_DELIVERED == LEVEL2)))
    /*
    * @violates @ref Spi_Lcfg_c_REF_3 The compiler/linker shall be checked to ensure that 31 character
    * signifiance and case sensitivity are supported for external identifiers.
    */
    #define SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
#else
    /*
    * @violates @ref Spi_Lcfg_c_REF_3 The compiler/linker shall be checked to ensure that 31 character
    * signifiance and case sensitivity are supported for external identifiers.
    */
    #define SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED
#endif /* ((SPI_DMA_USED == STD_ON) && ((SPI_LEVEL_DELIVERED == LEVEL1) ||
        (SPI_LEVEL_DELIVERED == LEVEL2))) */

/*
* @violates @ref Spi_Lcfg_c_REF_1 #include statements in a file should only be preceded
*        by other preprocessor directives or comments.
* @violates @ref Spi_Lcfg_c_REF_2 Precautions shall be taken in order to prevent the contents
*        of a header file being included twice.
*/
#include "MemMap.h"



/*  Buffers Descriptors for EB Channels (if any) */
/* Allocate Buffers for IB Channels (if any) */
static VAR(Spi_DataType, SPI_VAR) BufferTX_LTSpiChannel_0[8];
static VAR(Spi_DataType, SPI_VAR) BufferRX_LTSpiChannel_0[8];


#if ((SPI_DMA_USED == STD_ON) && \
    ((SPI_LEVEL_DELIVERED == LEVEL1) || (SPI_LEVEL_DELIVERED == LEVEL2)))
    /*
    * @violates @ref Spi_Lcfg_c_REF_3 The compiler/linker shall be checked to ensure that 31 character
    * signifiance and case sensitivity are supported for external identifiers.
    */
    #define SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
#else
    /*
    * @violates @ref Spi_Lcfg_c_REF_3 The compiler/linker shall be checked to ensure that 31 character
    * signifiance and case sensitivity are supported for external identifiers.
    */
    #define SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#endif /* ((SPI_DMA_USED == STD_ON) && ((SPI_LEVEL_DELIVERED == LEVEL1) ||
        (SPI_LEVEL_DELIVERED == LEVEL2))) */

/*
* @violates @ref Spi_Lcfg_c_REF_1 #include statements in a file should only be preceded
*        by other preprocessor directives or comments.
* @violates @ref Spi_Lcfg_c_REF_2 Precautions shall be taken in order to prevent the contents
*        of a header file being included twice.
*/
#include "MemMap.h"

#define SPI_START_SEC_VAR_INIT_UNSPECIFIED
/*
* @violates @ref Spi_Lcfg_c_REF_1 #include statements in a file should only be preceded
*        by other preprocessor directives or comments.
* @violates @ref Spi_Lcfg_c_REF_2 Precautions shall be taken in order to prevent the contents
*        of a header file being included twice.
*/
#include "MemMap.h"



/*  Buffers Descriptors for IB Channels (if any) */
static VAR(Spi_BufferDescriptorType, SPI_VAR) Buffer_LTSpiChannel_0 =
{
    BufferTX_LTSpiChannel_0,
    BufferRX_LTSpiChannel_0
};



    
    
    



    
            



    
        

                                    

/* LPspiDeviceAttributesConfig_LT Device Attribute Configuration of SpiDriver_0*/
static VAR(Spi_Ipw_LPspiDeviceAttributesConfigType, SPI_VAR) LPspiDeviceAttributesConfig_LT[1] =
{
                
    {
                                                                        /* SpiExternalDevice_0 */
        
                
            (uint32)(LPSPI_TCR_CPOL_HIGH_U32 | /* Clock Polarity (Idle State) */
                            LPSPI_TCR_CPHA_LEADING_U32 | /* Clock Phase */  
            LPSPI_TCR_PRESCALE_1_U32 /* Baudrate: Should=100000.0, Is=100000, Error=0.0% */
          |  LPSPI_TCR_BYSW_DIS_U32 
          | 
                                                (uint32)(LPSPI_TCR_PCS0_EN_U32 | /* Chip Select Pin Via Peripheral Engine*/
                                    LPSPI_TCR_CONT_DIS_U32)/* Disable continuous chip select */
                                                            
) & LPSPI_TCR_RESERVED_MASK_U32,
            ( ((uint32)(3) << 24u) | /* TimeClk2Cs: Should=1000ns, Is=1000, Error=0.0% */
            ((uint32)(3) << 16u) | /* TimeCs2Clk: Should=1000ns, Is=1000, Error=0.0% */
            ((uint32)(3) << 8u) | /* TimeCs2Cs: Should=1000ns, Is=1000, Error=0.0% */
            ((uint32)(38))
            ),

/* CFG1 register configuration */
/* Enable QSPI interface */
#if(SPI_QSPI_COMMUNICATION_ENABLE == STD_ON)
    LPSPI_CFGR1_PCSCFG_MASK_U32 |
#endif
        (uint32)0u  /* Chip select polarity */
     | LPSPI_CFGR1_PINCFG_SIN_INPUT_SOUT_OUTPUT_U32 | LPSPI_CFGR1_MATCFG_DIS_U32 | LPSPI_CFGR1_MASTER_EN_U32


    }        

};

    
    
    

#define SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
/*
* @violates @ref Spi_Lcfg_c_REF_1 #include statements in a file should only be preceded
*        by other preprocessor directives or comments.
* @violates @ref Spi_Lcfg_c_REF_2 Precautions shall be taken in order to prevent the contents
*        of a header file being included twice.
*/
#include "MemMap.h"
/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/
#define   SPI_START_SEC_VAR_INIT_UNSPECIFIED
/*
* @violates @ref Spi_Lcfg_c_REF_1 #include statements in a file should only be preceded
*        by other preprocessor directives or comments.
* @violates @ref Spi_Lcfg_c_REF_2 Precautions shall be taken in order to prevent the contents
*        of a header file being included twice.
*/
 #include "MemMap.h"


    
    

/* DeviceAttributesConfig_PB0 Device Attribute Configuration of SpiDriver_0*/
static P2CONST(Spi_Ipw_LPspiDeviceAttributesConfigType, SPI_VAR, SPI_APPL_CONST) LPspiDeviceAttributesConfigArray_LT[1] =
{
                &LPspiDeviceAttributesConfig_LT[0]        
};

    

/* DeviceAttributesConfig_PB0 Device Attribute Configuration of SpiDriver_0*/
static P2CONST(Spi_Ipw_FlexIODeviceAttributesConfigType, SPI_VAR, SPI_APPL_CONST) FlexIODeviceAttributesConfigArray_LT[1] =
{
                NULL_PTR
};





/* DSpiDeviceAttributesConfig_PB0 Device Attribute Configuration of SpiDriver_0*/
static VAR(Spi_Ipw_DeviceAttributesConfigType, SPI_VAR) SpiExternalDeviceAttrsLT[1] =
{
        
        
        {
        &LPspiDeviceAttributesConfigArray_LT[SPI_SpiExternalDevice_0],
        NULL_PTR
        }
};
 
 #define   SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
/*
* @violates @ref Spi_Lcfg_c_REF_1 #include statements in a file should only be preceded
*        by other preprocessor directives or comments.
* @violates @ref Spi_Lcfg_c_REF_2 Precautions shall be taken in order to prevent the contents
*        of a header file being included twice.
*/
 #include "MemMap.h"
 
#define SPI_START_SEC_CONFIG_DATA_UNSPECIFIED
/*
* @violates @ref Spi_Lcfg_c_REF_3 The compiler/linker shall be checked to ensure that 31 character
* signifiance and case sensitivity are supported for external identifiers.
*/
#define SPI_START_SEC_CONFIG_DATA_UNSPECIFIED
/*
* @violates @ref Spi_Lcfg_c_REF_1 #include statements in a file should only be preceded
*        by other preprocessor directives or comments.
* @violates @ref Spi_Lcfg_c_REF_2 Precautions shall be taken in order to prevent the contents
*        of a header file being included twice.
*/
#include "MemMap.h"



/* SpiChannelConfig_LT Channel Configuration of SpiDriver_0*/
static CONST(Spi_ChannelConfigType, SPI_CONST) SpiChannelConfig_LT[1] =
{
    {
        /* SpiChannel_0*/
        IB,
        (uint32)255U,

        8U,
        &Buffer_LTSpiChannel_0,
        &Spi_aSpiChannelState[0]
    }


};



/* Channel to Job Assignment */

static CONST(Spi_ChannelType, SPI_CONST) SpiJob_0_ChannelAssignment_LT[1] = {SpiConf_SpiChannel_SpiChannel_0};



static CONST(Spi_JobConfigType, SPI_CONST) SpiJobConfig_LT[1] =
{
    {
        /* SpiJob_0 */
        (Spi_ChannelType)1u,
        SpiJob_0_ChannelAssignment_LT, /* List of Channels */
        &Spi_Job0_EndNotification, /* End Notification */
        NULL_PTR, /* Start Notification */
        (sint8)0, /* Priority */
        &Spi_aSpiJobState[0], /* JobState instance */
        CSIB0, /* HWUnit index */
        LPSPI_0_OFFSET, /* LPspi device HW unit offset */
       /* External Device Settings */
        
            SPI_SpiExternalDevice_0, /* External Device */
        &SpiExternalDeviceAttrsLT[SPI_SpiExternalDevice_0]
        
    }


};



/* Job to Sequence Assignment */
static CONST(Spi_JobType, SPI_CONST) SpiSequence_0_JobAssignment_LT[1] = {SpiConf_SpiJob_SpiJob_0};



static CONST(Spi_SequenceConfigType, SPI_CONST) SpiSequenceConfig_LT[1] =
{
    {  /* SpiSequence_0 */
        LPSPI, /* HW type of sequences */ 
        (Spi_JobType)1u,
        SpiSequence_0_JobAssignment_LT, /* List of Jobs */
        &Spi_Seq0_EndNotification, /* End Notification */
                (uint8)FALSE /* Interruptible */
    }


};








    
            




    
/* LPspiChannelAttributesConfig_LT Channel Attribute Configuration of SpiDriver_0*/
static CONST(Spi_Ipw_LPspiChannelAttributesConfigType, SPI_CONST) LPspiChannelAttributesConfig_LT[1] =
{
{ 
    /* Channel attribution on TCR register */
    (uint32)(LPSPI_TCR_MSB_U32 | LPSPI_TCR_WIDTH_1_U32 | ((uint32)(7.0))),
    /* FCR register attribution */
    (uint32)(((uint32)(0) << 16) | ((uint32)(0))),SPI_DATA_WIDTH_8}



};










    
            



/* SpiAttributesConfig_LT Attribute Configuration of SpiDriver_0 */
static CONST(Spi_AttributesConfigType, SPI_CONST) SpiAttributesConfig_LT = {    
    LPspiChannelAttributesConfig_LT,
    NULL_PTR,
    LPspiDeviceAttributesConfigArray_LT,
    FlexIODeviceAttributesConfigArray_LT,
};



/* Array of LPspi Unit configurations */
static CONST(Spi_HWUnitConfigType, SPI_CONST) HWUnitConfig_LT[SPI_MAX_HWUNIT] =
{




    { LPSPI_0_OFFSET,  LPSPI, (uint8)0u, SPI_MASTER , SPI_PHYUNIT_ASYNC_U32, 0u, 0u, 0u, 0u }
};

/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/




/*
* @violates @ref Spi_Lcfg_c_REF_4 All declarations 
* and definitions of objects or functions at file scope shall have internal linkage unless external linkage is required.
*/
/* SpiDriver_0 Configuration */
CONST(Spi_ConfigType, SPI_CONST) SpiDriverConfig_LT =
{
    1u,
    0u,
    0u,
    0u,
    SpiChannelConfig_LT,
    SpiJobConfig_LT,
    SpiSequenceConfig_LT,
    &SpiAttributesConfig_LT,
    HWUnitConfig_LT,
    /**
    * @brief   DEM error parameters
    */
#if (SPI_DISABLE_DEM_REPORT_ERROR_STATUS == STD_OFF)
    { (uint32)STD_OFF, (uint32)0U}/* SPI_E_HARDWARE_ERROR parameters*/ 

#endif /* SPI_DISABLE_DEM_REPORT_ERROR_STATUS == STD_OFF */  
};


#define SPI_STOP_SEC_CONFIG_DATA_UNSPECIFIED
/*
* @violates @ref Spi_Lcfg_c_REF_1 #include statements in a file should only be preceded
*        by other preprocessor directives or comments.
* @violates @ref Spi_Lcfg_c_REF_2 Precautions shall be taken in order to prevent the contents
*        of a header file being included twice.
*/
#include "MemMap.h"


#endif /*(SPI_CONFIG_VARIANT == SPI_VARIANT_LINKTIME)*/

#ifdef __cplusplus
}
#endif

/** @} */

