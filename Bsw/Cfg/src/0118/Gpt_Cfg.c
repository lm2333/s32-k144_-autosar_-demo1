/**
*   @file    Gpt_Cfg.c
*   @implements     Gpt_Cfg.c_Artifact
*   @version 1.0.4
*
*   @brief   AUTOSAR Gpt -  GPT driver configuration source file..
*   @details GPT driver source file, containing C and XPath constructs for generating Gpt
*            configuration source file for the Precompile configuration variant.
*
*   @addtogroup GPT_MODULE
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.0 MCAL
*   Platform             : ARM
*   Peripheral           : Ftm
*   Dependencies         : none
*
*   Autosar Version      : 4.0.3
*   Autosar Revision     : ASR_REL_4_0_REV_0003
*   Autosar Conf.Variant :
*   SW Version           : 1.0.4
*   Build Version        : S32K14xS32K14x_MCAL_1_0_4_RTM_ASR_REL_4_0_REV_0003_20190307
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2018-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifdef __cplusplus
extern "C"{
#endif

/**
* @page misra_violations MISRA-C:2004 violations
*
* @section [global]
*     Violates MISRA 2004 Required Rule 5.1, Identifiers (internal and external) shall not rely 
*     on the significance of more than 31 characters. The used compilers use more than 31 chars for
*     identifiers.
*
* @section GPT_CFG_C_REF_1
* Violates MISRA 2004 Advisory Rule 19.1, only preprocessor statements
* and comments before '#include'. This is an Autosar requirement about
* the memory management.
*
* @section GPT_CFG_C_REF_2
* Violates MISRA 2004 Required Rule 19.15, Precautions shall be taken in order to 
* prevent the contents of a header file being included twice. This is an Autosar requirement 
* about the memory management.
*
* @section GPT_CFG_C_REF_3
* Violates MISRA 2004 Required Rule 1.4, The compiler/linker shall be checked to ensure 
* that 31 character significance and case sensitivity are supported for external identifiers.
* Compilers and linkers checked. Feature is supported 
*
* @section GPT_CFG_C_REF_4
* Violates MISRA 2004 Required Rule 8.10, could be made static
* The respective code could not be made static because of layers architecture design of the
* driver.
*/


/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcal.h"
#include "Gpt.h"

#if (GPT_PRECOMPILE_SUPPORT == STD_ON)
/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @file           Gpt_Cfg.c
* @{
*/
#define GPT_VENDOR_ID_CFG_C                    43
/** @violates @ref GPT_CFG_C_REF_3 MISRA 2004 Rule 1.4, The compiler/linker shall be checked to ensure that 31 character
*   significance and case sensitivity are supported for external identifiers. */
#define GPT_AR_RELEASE_MAJOR_VERSION_CFG_C     4
/** @violates @ref GPT_CFG_C_REF_3 MISRA 2004 Rule 1.4, The compiler/linker shall be checked to ensure that 31 character
*   significance and case sensitivity are supported for external identifiers. */
#define GPT_AR_RELEASE_MINOR_VERSION_CFG_C     0
/** @violates @ref GPT_CFG_C_REF_3 MISRA 2004 Rule 1.4, The compiler/linker shall be checked to ensure that 31 character
*   significance and case sensitivity are supported for external identifiers. */
#define GPT_AR_RELEASE_REVISION_VERSION_CFG_C  3
#define GPT_SW_MAJOR_VERSION_CFG_C             1
#define GPT_SW_MINOR_VERSION_CFG_C             0
#define GPT_SW_PATCH_VERSION_CFG_C             4
/** @} */

/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/

#ifndef DISABLE_MCAL_INTERMODULE_ASR_CHECK 
    #if ((GPT_AR_RELEASE_MAJOR_VERSION_CFG_C != MCAL_AR_RELEASE_MAJOR_VERSION) || \
         (GPT_AR_RELEASE_MINOR_VERSION_CFG_C != MCAL_AR_RELEASE_MINOR_VERSION))
        #error "AutoSar Version Numbers of Gpt_Cfg.c and Mcal.h are different"
    #endif   
#endif




/* Check if Gpt_Cfg.c file and Gpt.h header file are of the same vendor */
#if (GPT_VENDOR_ID_CFG_C != GPT_VENDOR_ID)
    #error "Gpt_Cfg.c and Gpt.h have different vendor ids"
#endif
/* Check if Gpt_Cfg.c file and Gpt.h header file are of the same Autosar version */
#if ((GPT_AR_RELEASE_MAJOR_VERSION_CFG_C != GPT_AR_RELEASE_MAJOR_VERSION) || \
     (GPT_AR_RELEASE_MINOR_VERSION_CFG_C != GPT_AR_RELEASE_MINOR_VERSION) || \
     (GPT_AR_RELEASE_REVISION_VERSION_CFG_C != GPT_AR_RELEASE_REVISION_VERSION))
    #error "AutoSar Version Numbers of Gpt_Cfg.c and Gpt.h are different"
#endif
/* Check if Gpt_Cfg.c file and Gpt.h header file are of the same Software version */
#if ((GPT_SW_MAJOR_VERSION_CFG_C != GPT_SW_MAJOR_VERSION) || \
     (GPT_SW_MINOR_VERSION_CFG_C != GPT_SW_MINOR_VERSION) || \
     (GPT_SW_PATCH_VERSION_CFG_C != GPT_SW_PATCH_VERSION))
    #error "Software Version Numbers of Gpt_Cfg.c and Gpt.h are different"
#endif

/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/
#define GPT_START_SEC_CODE
/** @violates @ref GPT_CFG_C_REF_1 Only preprocessor statements and comments before '#include' */
/** @violates @ref GPT_CFG_C_REF_2 MISRA 2004 Required Rule 19.15, precautions to prevent the 
*   contents of a header file being included twice.*/
#include "MemMap.h"
/**
* @{
* @brief The callback functions defined by the user to be called as channel notifications 
*/
extern void SampleAppGptLed(void);
extern void Wdg_Cbk_GptNotification0(void);
/** @} */
#define GPT_STOP_SEC_CODE
/** @violates @ref GPT_CFG_C_REF_1 Only preprocessor statements and comments before '#include' */
/** @violates @ref GPT_CFG_C_REF_2 MISRA 2004 Required Rule 19.15, precautions to prevent the 
*   contents of a header file being included twice.*/
#include "MemMap.h"
/*==================================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/

/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/

/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/

/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/

/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/
#define GPT_START_SEC_CONFIG_DATA_UNSPECIFIED
/** @violates @ref GPT_CFG_C_REF_1 Only preprocessor statements and comments before '#include' */
/** @violates @ref GPT_CFG_C_REF_2 MISRA 2004 Required Rule 19.15, precautions to prevent the 
*   contents of a header file being included twice.*/
#include "MemMap.h"






static CONST(Gpt_ChannelConfigType, GPT_CONST) Gpt_InitChannelPC[2]=
{
    {   /*GptChannelConfiguration_LedBlinker configuration data*/
        (boolean)FALSE, /* Wakeup capability */
        &SampleAppGptLed, /* Channel notification */
#if ((GPT_WAKEUP_FUNCTIONALITY_API == STD_ON) && (GPT_REPORT_WAKEUP_SOURCE == STD_ON))
        (EcuM_WakeupSourceType)0U, /* Wakeup information */
#endif
        (Gpt_ValueType)(4294967295U), /* Maximum ticks value*/
        (Gpt_ChannelModeType)(GPT_CH_MODE_CONTINUOUS), /* Timer mode:continous/one-shot */
        {
            (uint8)(LPIT_0_CH_0), /* GPT physical channel no. */
            (uint8)(GPT_LPIT_MODULE), /* hardware module ID */
            (boolean)TRUE, /* Freeze Enable */
            (uint8)(0U), /* FTM Clock source, FTM is not used */
            (Gpt_PrescalerType)(0U), /* FTM Clock divider, FTM is not used */
            (uint8)(0U), /* LPTMR Clock divider, LPTMR is not USED */
            (Gpt_PrescalerType)(0U),  /* LPTMR Clock Select, LPTMR is not USED */
#if (GPT_SET_CLOCK_MODE == STD_ON)
            (Gpt_PrescalerType)(0U), /* FTM alternate Clock divider, FTM is not USED */
            (Gpt_PrescalerType)(0U), /* LPTMR alternate Clock divider, LPTMR is not USED */
#endif /*GPT_SET_CLOCK_MODE == STD_ON*/

            (uint8)0             /* SRTC Clock Select, SRTC is not USED */

#if (GPT_LPIT_ENABLE_EXT_TRIGGERS==STD_ON)
/* LPIT External/Internal Trigger Configuration */
            ,(uint32)0U | \
                (uint32)((uint32)1U<<23U) | \
                (uint32)((uint32)0U<<18U) | \
                (uint32)((uint32)0U<<17U) | \
                (uint32)((uint32)0U<<16U)
#endif
        }
    }

,
    {   /*GptChannelConfiguration_WdgTrig configuration data*/
        (boolean)FALSE, /* Wakeup capability */
        &Wdg_Cbk_GptNotification0, /* Channel notification */
#if ((GPT_WAKEUP_FUNCTIONALITY_API == STD_ON) && (GPT_REPORT_WAKEUP_SOURCE == STD_ON))
        (EcuM_WakeupSourceType)0U, /* Wakeup information */
#endif
        (Gpt_ValueType)(4294967295U), /* Maximum ticks value*/
        (Gpt_ChannelModeType)(GPT_CH_MODE_CONTINUOUS), /* Timer mode:continous/one-shot */
        {
            (uint8)(LPIT_0_CH_1), /* GPT physical channel no. */
            (uint8)(GPT_LPIT_MODULE), /* hardware module ID */
            (boolean)TRUE, /* Freeze Enable */
            (uint8)(0U), /* FTM Clock source, FTM is not used */
            (Gpt_PrescalerType)(0U), /* FTM Clock divider, FTM is not used */
            (uint8)(0U), /* LPTMR Clock divider, LPTMR is not USED */
            (Gpt_PrescalerType)(0U),  /* LPTMR Clock Select, LPTMR is not USED */
#if (GPT_SET_CLOCK_MODE == STD_ON)
            (Gpt_PrescalerType)(0U), /* FTM alternate Clock divider, FTM is not USED */
            (Gpt_PrescalerType)(0U), /* LPTMR alternate Clock divider, LPTMR is not USED */
#endif /*GPT_SET_CLOCK_MODE == STD_ON*/

            (uint8)0             /* SRTC Clock Select, SRTC is not USED */

#if (GPT_LPIT_ENABLE_EXT_TRIGGERS==STD_ON)
/* LPIT External/Internal Trigger Configuration */
            ,(uint32)0U | \
                (uint32)((uint32)1U<<23U) | \
                (uint32)((uint32)0U<<18U) | \
                (uint32)((uint32)0U<<17U) | \
                (uint32)((uint32)0U<<16U)
#endif
        }
    }




};
/*Precompile configuration structure.*/
/** @violates @ref GPT_CFG_C_REF_4 Violates MISRA 2004 Required Rule 8.10 could be made static*/
CONST(Gpt_ConfigType, GPT_CONST) Gpt_InitConfigPC =
{
    (Gpt_ChannelType)2U,
    &Gpt_InitChannelPC,
    /*Hardware to logic channel mapping.*/
    {
/* The maximum number of channels per module for FTM */
#define GPT_FTM_CHANNELS_PER_MODULE_U8 (8U)

        GPT_CHN_NOT_USED
                , /*mapping of FTM_0_CH_0*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_0_CH_1*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_0_CH_2*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_0_CH_3*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_0_CH_4*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_0_CH_5*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_0_CH_6*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_0_CH_7*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_1_CH_0*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_1_CH_1*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_1_CH_2*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_1_CH_3*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_1_CH_4*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_1_CH_5*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_1_CH_6*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_1_CH_7*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_2_CH_0*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_2_CH_1*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_2_CH_2*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_2_CH_3*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_2_CH_4*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_2_CH_5*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_2_CH_6*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_2_CH_7*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_3_CH_0*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_3_CH_1*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_3_CH_2*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_3_CH_3*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_3_CH_4*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_3_CH_5*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_3_CH_6*/
        GPT_CHN_NOT_USED
                , /*mapping of FTM_3_CH_7*/
/* The maximum number of channels per module for LPIT */
#define GPT_LPIT_CHANNELS_PER_MODULE_U8 (4U)

        GptConf_GptChannelConfiguration_GptChannelConfiguration_LedBlinker
                        , /*mapping of LPIT_0_CH_0*/
        GptConf_GptChannelConfiguration_GptChannelConfiguration_WdgTrig
                        , /*mapping of LPIT_0_CH_1*/
        GPT_CHN_NOT_USED
                , /*mapping of LPIT_0_CH_2*/
        GPT_CHN_NOT_USED
                , /*mapping of LPIT_0_CH_3*/
/* The maximum number of channels per module for LPTMR */
#define GPT_LPTMR_CHANNELS_PER_MODULE_U8 (1U)

        GPT_CHN_NOT_USED
                , /*mapping of LPTMR_0_CH_0*/
/* The maximum number of channels per module for SRTC */
#define GPT_SRTC_CHANNELS_PER_MODULE_U8 (1U)

        GPT_CHN_NOT_USED
                 /*mapping of SRTC_0_CH_0*/
    }
};

#define GPT_STOP_SEC_CONFIG_DATA_UNSPECIFIED
/** @violates @ref GPT_CFG_C_REF_1 Only preprocessor statements and comments before '#include' */
/** @violates @ref GPT_CFG_C_REF_2 MISRA 2004 Required Rule 19.15, precautions to prevent the 
*   contents of a header file being included twice.*/
#include "MemMap.h"


/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/

/*==================================================================================================
*                                       LOCAL FUNCTIONS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/

#endif  /* GPT_PRECOMPILE_SUPPORT */

#ifdef __cplusplus
}
#endif

/** @} */
