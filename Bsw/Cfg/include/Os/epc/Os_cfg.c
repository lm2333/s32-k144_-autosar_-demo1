/******************************************************************************
*
*       NXP(TM) and the NXP logo are trademarks of NXP.
*       All other product or service names are the property of their respective owners.
*       (C) Freescale Semiconductor, Inc. 2013-2016
*       Copyright 2022 NXP
*
*       THIS SOURCE CODE IS CONFIDENTIAL AND PROPRIETARY AND MAY NOT
*       BE USED OR DISTRIBUTED WITHOUT THE WRITTEN PERMISSION OF NXP.
*
*       Description: Configuration Data file
*
*       Note: The implementation that was used is: AUTOSAR_S32K
*       System Generator for AUTOSAR OS/S32K - Version: 4.0 Build 4.0.98
*
********************************************************************************/
#include    <Os.h>
#include    <Os_internal_config.h>
#define OSTEXT_START_SEC_CODE
#include    "Os_sections.h"

#define OS_START_SEC_CONST_UNSPECIFIED
#include    "Os_sections.h"

const    OSAPP   OsAppCfgTable[OSNAPPS] = 
{
    {
        0xf0000000U, /* all tasks of the application, priority-wise */
    }, /* TRUSTED_APP */
};

/* Task Configuration table */
const    OSTSK   OsTaskCfgTable[OSNTSKS] = 
{
    {
        1U, /* Application identification mask value */
        (OSTASKENTRY) &FuncOsTask_CanNmTest, /* entry point of task */
        0U, /* properties of task OSTSKACTIVATE, OSTSKEXTENDED, OSTSKNONPREMPT, OSTSKFLOATINGPOINT */
        0U, /* task id (task number in the task table) */
        0U, /* application identification value */
    }, /* OsTask_CanNmTest */
    {
        1U, /* Application identification mask value */
        (OSTASKENTRY) &FuncOsTask_FeeTest, /* entry point of task */
        0U, /* properties of task OSTSKACTIVATE, OSTSKEXTENDED, OSTSKNONPREMPT, OSTSKFLOATINGPOINT */
        1U, /* task id (task number in the task table) */
        0U, /* application identification value */
    }, /* OsTask_FeeTest */
    {
        1U, /* Application identification mask value */
        (OSTASKENTRY) &FuncTASKLED, /* entry point of task */
        0U | OSTSKNONPREEMPT, /* properties of task OSTSKACTIVATE, OSTSKEXTENDED, OSTSKNONPREMPT, OSTSKFLOATINGPOINT */
        2U, /* task id (task number in the task table) */
        0U, /* application identification value */
    }, /* TASKLED */
    {
        1U, /* Application identification mask value */
        (OSTASKENTRY) &FuncOsTask_5ms, /* entry point of task */
        0U | OSTSKNONPREEMPT, /* properties of task OSTSKACTIVATE, OSTSKEXTENDED, OSTSKNONPREMPT, OSTSKFLOATINGPOINT */
        3U, /* task id (task number in the task table) */
        0U, /* application identification value */
    }, /* OsTask_5ms */
};
const    OSISRCFGTYPE   OsIsrCfg[OSNISR + 1] =  /*Interrupts config table*/
{
    {
        OSISRSystemTimer, /* actual ISR function */
        OSSYSINTERRUPT, /* ISR type */
        115U, /* index in OsIsr */
        2U, /* Interrupt priority */
        OSINVALID_OSAPPLICATION, /* appId */
    }, /* SysTimer */
    {
        OSISRSecondTimer, /* actual ISR function */
        OSSYSINTERRUPT, /* ISR type */
        15U, /* index in OsIsr */
        4U, /* Interrupt priority */
        OSINVALID_OSAPPLICATION, /* appId */
    }, /* SecondTimer */
    {
        OSISRException, /* actual ISR function */
        OSSYSINTERRUPT, /* ISR type */
        OSISREXCEPTIONIDX, /* Index of interrupt */
        OSISREXCEPTIONPRIO, /* Interrupt priority */
        OSINVALID_OSAPPLICATION, /* appId */
    }, /* Exception */
};
const    OSSHORT   OsIsr[OSNINTC] =  /* OSNINTERRUPTS external interrupt handlers table */
{
   /*  0*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /* 10*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,     1U, OSNISR, OSNISR, OSNISR, OSNISR,
   /* 20*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /* 30*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /* 40*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /* 50*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /* 60*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /* 70*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /* 80*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /* 90*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /*100*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /*110*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,     0U, OSNISR, OSNISR, OSNISR, OSNISR,
   /*120*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /*130*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /*140*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /*150*/ OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR, OSNISR,
   /*160*/ OSNISR, OSNISR, OSNISR,
};

const    OSRESCFG   OsResCfg[OSNRESS + OSNISRRESS] = 
{
    { 0xFFU, /* Application identification mask value */0U, /* Resource priority for task resources */ }, /* RES_SCHEDULER */
};

/* Alarms table */
const    OSALM   OsAlarmsCfg[OSNUSERALMS] = 
{
    {
        1U, /* appMask */
        3U, /* task to notify */
        1U, /* attached Counter ID */
        0U, /* application identification value */
    }, /* OsAlarm_5ms */
};

/* Auto started Alarms */
const    OSALMAUTOTYPE   OsAutoAlarms[OSNAUTOALMS] = 
{
    {
        &OsAllAlarms.OsAlarms[0U], /* Reference to alarm */
        (TickType)0U, /* Time to start (relative) */
        (TickType)5U, /* Alarm cycle, 0U for non-cycled */
        OSALMABSOLUTE, /* The type of autostart alarm */
    }, /* OsAlarm_5ms */
};

/* Counter table */
const    OSCTR   OsCountersCfg[OSNCTRS] = 
{
    {
        1U, /* appMask */
        (TickType) (0xffffU), /* maximum allowed counter value */
        1000U, /* conversion constant */
        (TickType) (0x1U), /* minimum period value for alarm */
        0U, /* application identification value */
    }, /* SYSTEMTIMER */
    {
        1U, /* appMask */
        (TickType) (0x7530U), /* maximum allowed counter value */
        10U, /* conversion constant */
        (TickType) (0x1U), /* minimum period value for alarm */
        0U, /* application identification value */
    }, /* SECONDTIMER */
};
static const    OSSCTEP   OSSCHEDTAB_actions[] = 
{
    {
        (OSSCTEPREF) &OSSCHEDTAB_actions[1U], /* the next expiry point */
        { 2U, /* Task to start or to set Event */ }, /* action */
        15000U, /* Delay to the next expiry point */
    }, /* TASKLED TASKACTIVATION */
    {
        (OSSCTEPREF) &OSSCHEDTAB_actions[0U], /* the next expiry point */
        { 0U, /* Task to start or to set Event */ }, /* action */
        21536U, /* Delay to the next expiry point */
    }, /* OsTask_CanNmTest TASKACTIVATION */
};
const    OSSCT   OsScheduleTablesCfg[OSNSCTS] = 
{
    {
        0U, /* attached Counter ID */
        0U, /* Initial Offset of the Schedule Table */
        (OSSCTEPREF) &OSSCHEDTAB_actions[0U], /* the pointer to first expiry point in the Schedule Table */
        1U, /* Application identification mask value */
        0U, /* application identification value */
        OSSCTCFGPERIODIC, /* config byte of the Schedule Table */
    }, /* OSSCHEDTAB */
};
const    OSSCTAUTOTYPE   OsAutoScheduleTablesCfg[OSNAUTOSCTS] = 
{
    { 0U, /* sctIndex */1U, /* offset */OSSCTABSOLUTE, /* type */ }, /* SCHEDTAB */
};
#define OS_STOP_SEC_CONST_UNSPECIFIED
#include    "Os_sections.h"

#define OSTEXT_STOP_SEC_CODE
#include    "Os_sections.h"

