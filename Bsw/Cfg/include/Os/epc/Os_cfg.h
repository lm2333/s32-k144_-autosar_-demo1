/******************************************************************************
*
*       NXP(TM) and the NXP logo are trademarks of NXP.
*       All other product or service names are the property of their respective owners.
*       (C) Freescale Semiconductor, Inc. 2013-2016
*       Copyright 2022 NXP
*
*       THIS SOURCE CODE IS CONFIDENTIAL AND PROPRIETARY AND MAY NOT
*       BE USED OR DISTRIBUTED WITHOUT THE WRITTEN PERMISSION OF NXP.
*
*       Description: Configuration Header file
*
*       Note: The implementation that was used is: AUTOSAR_S32K
*       System Generator for AUTOSAR OS/S32K - Version: 4.0 Build 4.0.98
*
********************************************************************************/
#ifndef OSCFG_H
#define OSCFG_H
#define APP_START_SEC_CODE
#include    "Os_memmap.h"

#define OS_START_SEC_CONST_UNSPECIFIED
#include    "Os_sections.h"


/* Applications */
#define TRUSTED_APP ((ApplicationType)OS_MKOBJID(OBJECT_APPLICATION, 0U)) /* Application ID */

/* Spinlock */

/* Application modes */
#define Mode01 ((AppModeType)0U)           /* AppMode ID */

/* Common stack */

/* Task definitions */
#define OsTask_CanNmTest ((TaskType)OS_MKOBJID(OBJECT_TASK, 0U)) /* Task ID */
extern void FuncOsTask_CanNmTest(void); /* Task entry point */
#define OsTask_FeeTest ((TaskType)OS_MKOBJID(OBJECT_TASK, 1U)) /* Task ID */
extern void FuncOsTask_FeeTest(void); /* Task entry point */
#define TASKLED ((TaskType)OS_MKOBJID(OBJECT_TASK, 2U)) /* Task ID */
extern void FuncTASKLED(void); /* Task entry point */
#define OsTask_5ms ((TaskType)OS_MKOBJID(OBJECT_TASK, 3U)) /* Task ID */
extern void FuncOsTask_5ms(void); /* Task entry point */

/* ISR functions */

/* ISRs definition */

/* ISR1 id */

/* Resources definitions */
#define RES_SCHEDULER ((ResourceType)OS_MKOBJID(OBJECT_RESOURCE, 0U)) /* Resource ID */

/* Events definition */

/* Alarms identification */
#define OsAlarm_5ms ((AlarmType)OS_MKOBJID(OBJECT_ALARM, 0U)) /* Alarm ID */

/* Counters identification */
#define SYSTEMTIMER ((CounterType)OS_MKOBJID(OBJECT_COUNTER, 0U)) /* Counter ID */
#define OSMINCYCLE_SYSTEMTIMER ((TickType)0x1U) /* SYSTEMTIMER */
#define OSMAXALLOWEDVALUE_SYSTEMTIMER ((TickType)0xffffU) /* SYSTEMTIMER */
#define OSTICKSPERBASE_SYSTEMTIMER 1000UL  /* SYSTEMTIMER */
#define OS_TICKS2NS_SYSTEMTIMER(ticks) (PhysicalTimeType)(ticks*10666U) /*  */
#define OS_TICKS2US_SYSTEMTIMER(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*10666ULL/1000UL) /*  */
#define OS_TICKS2MS_SYSTEMTIMER(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*10666ULL/1000000UL) /*  */
#define OS_TICKS2SEC_SYSTEMTIMER(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*10666ULL/1000000000UL) /*  */
#define SECONDTIMER ((CounterType)OS_MKOBJID(OBJECT_COUNTER, 1U)) /* Counter ID */
#define OSMINCYCLE_SECONDTIMER ((TickType)0x1U) /* SECONDTIMER */
#define OSMAXALLOWEDVALUE_SECONDTIMER ((TickType)0x7530U) /* SECONDTIMER */
#define OSTICKSPERBASE_SECONDTIMER 10UL    /* SECONDTIMER */
#define OS_TICKS2NS_SECONDTIMER(ticks) (PhysicalTimeType)(ticks*3896000U) /*  */
#define OS_TICKS2US_SECONDTIMER(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*3896000ULL/1000UL) /*  */
#define OS_TICKS2MS_SECONDTIMER(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*3896000ULL/1000000UL) /*  */
#define OS_TICKS2SEC_SECONDTIMER(ticks) (PhysicalTimeType)((OSQWORD)(ticks)*3896000ULL/1000000000UL) /*  */
#define OSMINCYCLE ((TickType)0x1U)        /* SysTimer */
#define OSMAXALLOWEDVALUE ((TickType)0xffffU) /* SysTimer */
#define OSTICKSPERBASE 1000UL              /* SysTimer */
#define OSTICKDURATION 10666UL             /* SysTimer */
#define OSMINCYCLE2 ((TickType)0x1U)       /* SecondTimer */
#define OSMAXALLOWEDVALUE2 ((TickType)0x7530U) /* SecondTimer */
#define OSTICKSPERBASE2 10UL               /* SecondTimer */
#define OSTICKDURATION2 3896000UL          /* SecondTimer */

/* Messages identification */

/* Flags identification */

/* Message callback prototypes */

/* scheduletable */
#define SCHEDTAB ((ScheduleTableType)OS_MKOBJID(OBJECT_SCHEDULETABLE, 0U)) /* ScheduleTable ID */
#define OS_STOP_SEC_CONST_UNSPECIFIED
#include    "Os_sections.h"

#define APP_STOP_SEC_CODE
#include    "Os_memmap.h"

#endif /* OSCFG_H */

