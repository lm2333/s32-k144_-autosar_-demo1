/**
*   @file    Mcl_FlexIO_Common.c
*   @implements Mcl_FlexIO_Common.c_Artifact
*   @version 1.0.4
*
*   @brief   AUTOSAR Mcl - FLEXIO driver source file.
*   @details Common FlexIO routines.
*
*   @addtogroup FLEXIO_MODULE
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.0 MCAL
*   Platform             : ARM
*   Peripheral           : eDMA
*   Dependencies         : none
*
*   Autosar Version      : 4.0.3
*   Autosar Revision     : ASR_REL_4_0_REV_0003
*   Autosar Conf.Variant :
*   SW Version           : 1.0.4
*   Build Version        : S32K14xS32K14x_MCAL_1_0_4_RTM_ASR_REL_4_0_REV_0003_20190307
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2018-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/
    
#ifdef __cplusplus
extern "C"{
#endif
/**
* @page misra_violations MISRA-C:2012 violations
* @section [global]
* Violates MISRA 2012 Required Rule 5.1, External identifiers shall be distinct.
* Identifiers (internal and external) shall not rely on the significance of more than 31 characters.
* The used compilers use more than 31 chars for identifiers.
* @section [global]
* Violates MISRA 2012 Required Rule 5.2, Identifiers declared in the same scope and namespace shall be distinct.
* This violation is due to the requirement that requests to have a file version check.
*
* @section [global]
* Violates MISRA 2012 Required Rule 5.4, Macro identifiers shall be distinct.
* The used compilers use more than 31 chars for identifiers.
*
* @section [global]
* Violates MISRA 2012 Required Rule 5.5, Identifiers shall be distinct from macro names.
* The used compilers use more than 31 chars for identifiers.
*
* @section Mcl_FlexIO_Common_c_REF_1
* Violates MISRA 2012 Advisory Rule 11.4, A conversion should not be performed between a pointer to object and an integer type.
* The cast is used to access memory mapped registers.
*
* @section Mcl_FlexIO_Common_c_REF_2
* Violates MISRA 2012 Required Rule 11.6, A cast shall not be performed between pointer to void and an arithmetic type.
* This macro computes the address of any register by using the hardware offset of the controller. The address calculated as an unsigned int is passed to a macro for initializing the pointer with that address.
*
* @section Mcl_FlexIO_Common_c_REF_3
* Violates MISRA 2012 Advisory Rule 13.4, The result of an assignment operator should not be used.
* They are use to reduce code complexity.
*
* @section Mcl_FlexIO_Common_c_REF_4
* Violates MISRA 2012 Required Directive 4.10, Precautions shall be taken in order to prevent the contents of a header file being included more than once.
* This violation is not fixed since the inclusion of <MA>_MemMap.h is as per AUTOSAR requirement [SWS_MemMap_00003].
*
* @section Mcl_FlexIO_Common_c_REF_5
* Violates MISRA 2012 Advisory Rule 20.1, Include directives should only be preceded by preprocessor directives or comments.
* <MA>_MemMap.h is included after each section define in order to set the current memory section as defined by AUTOSAR.
*
**/
/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
/**
* @file           Mcl_FlexIO_Common.c
* 
*/
#include "Std_Types.h"
#include "SchM_Mcl.h"
#include "StdRegMacros.h"
#include "CDD_Mcl_Cfg.h"
#include "Mcl_FlexIO_Common_Types.h"
#include "Mcl_FlexIO_Common.h"
#include "Reg_eSys_FlexIO.h"
/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @{
* @file           Mcl_FlexIO_Common.c
* @brief          Source file version information  
*/
#define FLEXIO_COMMON_VENDOR_ID_C                       43
#define FLEXIO_COMMON_AR_RELEASE_MAJOR_VERSION_C        4
#define FLEXIO_COMMON_AR_RELEASE_MINOR_VERSION_C        0
#define FLEXIO_COMMON_AR_RELEASE_REVISION_VERSION_C     3
#define FLEXIO_COMMON_SW_MAJOR_VERSION_C                1
#define FLEXIO_COMMON_SW_MINOR_VERSION_C                0
#define FLEXIO_COMMON_SW_PATCH_VERSION_C                4
/**@}*/

/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/
/* Check if source file and Mcl_FlexIO_Common.h header file are of the same vendor */
#if (FLEXIO_COMMON_VENDOR_ID_C != FLEXIO_COMMON_VENDOR_ID)
    #error "Mcl_FlexIO_Common.c and Mcl_FlexIO_Common.h have different vendor ids"
#endif
/* Check if source file and Mcl_FlexIO_Common.h header file are of the same Autosar version */
#if ((FLEXIO_COMMON_AR_RELEASE_MAJOR_VERSION_C    != FLEXIO_COMMON_AR_RELEASE_MAJOR_VERSION) || \
     (FLEXIO_COMMON_AR_RELEASE_MINOR_VERSION_C    != FLEXIO_COMMON_AR_RELEASE_MINOR_VERSION) || \
     (FLEXIO_COMMON_AR_RELEASE_REVISION_VERSION_C != FLEXIO_COMMON_AR_RELEASE_REVISION_VERSION))
    #error "AutoSar Version Numbers of Mcl_FlexIO_Common.h and Mcl_FlexIO_Common.h are different"
#endif
/* Check if source file and Mcl_FlexIO_Common.h header file are of the same software version */
#if ((FLEXIO_COMMON_SW_MAJOR_VERSION_C != FLEXIO_COMMON_SW_MAJOR_VERSION) || \
     (FLEXIO_COMMON_SW_MINOR_VERSION_C != FLEXIO_COMMON_SW_MINOR_VERSION) || \
     (FLEXIO_COMMON_SW_PATCH_VERSION_C != FLEXIO_COMMON_SW_PATCH_VERSION))
    #error "Software Version Numbers of Mcl_FlexIO_Common.h and Mcl_FlexIO_Common.h are different"
#endif

/* Check if source file and Mcl_FlexIO_Common.h header file are of the same vendor */
#if (FLEXIO_COMMON_VENDOR_ID_C != FLEXIO_COMMON_TYPES_VENDOR_ID)
    #error "Mcl_FlexIO_Common.c and Mcl_FlexIO_Common.h have different vendor ids"
#endif
/* Check if source file and Mcl_FlexIO_Common.h header file are of the same Autosar version */
#if ((FLEXIO_COMMON_AR_RELEASE_MAJOR_VERSION_C    != FLEXIO_COMMON_TYPES_AR_RELEASE_MAJOR_VERSION) || \
     (FLEXIO_COMMON_AR_RELEASE_MINOR_VERSION_C    != FLEXIO_COMMON_TYPES_AR_RELEASE_MINOR_VERSION) || \
     (FLEXIO_COMMON_AR_RELEASE_REVISION_VERSION_C != FLEXIO_COMMON_TYPES_AR_RELEASE_REVISION_VERSION))
    #error "AutoSar Version Numbers of Mcl_FlexIO_Common.h and Mcl_FlexIO_Common.h are different"
#endif
/* Check if source file and Mcl_FlexIO_Common.h header file are of the same software version */
#if ((FLEXIO_COMMON_SW_MAJOR_VERSION_C != FLEXIO_COMMON_TYPES_SW_MAJOR_VERSION) || \
     (FLEXIO_COMMON_SW_MINOR_VERSION_C != FLEXIO_COMMON_TYPES_SW_MINOR_VERSION) || \
     (FLEXIO_COMMON_SW_PATCH_VERSION_C != FLEXIO_COMMON_TYPES_SW_PATCH_VERSION))
    #error "Software Version Numbers of Mcl_FlexIO_Common.h and Mcl_FlexIO_Common.h are different"
#endif

/* Check if source file and StdRegMacros header file are of the same Autosar version */
#ifndef DISABLE_MCAL_INTERMODULE_ASR_CHECK
    #if ((FLEXIO_COMMON_AR_RELEASE_MAJOR_VERSION_C != STDREGMACROS_AR_RELEASE_MAJOR_VERSION) || \
         (FLEXIO_COMMON_AR_RELEASE_MINOR_VERSION_C != STDREGMACROS_AR_RELEASE_MINOR_VERSION))
            #error "AutoSar Version Numbers of Mcl_Dma_Irq.cand StdRegMacros.h are different"
    #endif
#endif
/*==================================================================================================
*                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/

/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/

/*==================================================================================================
*                                       LOCAL FUNCTIONS
==================================================================================================*/

/*==================================================================================================
*                                       GLOBAL FUNCTIONS
==================================================================================================*/
#if (MCL_ENABLE_FLEXIO == STD_ON)
#define MCL_START_SEC_CODE
/* @violates @ref Mcl_FlexIO_Common_c_REF_4 MISRA 2012 Required 4.10*/
/* @violates @ref Mcl_FlexIO_Common_c_REF_5 MISRA 2012 Advisory 20.1*/
#include "MemMap.h"

FUNC (void, MCL_CODE) Mcl_Flexio_Init(VAR(uint32, AUTOMATIC) u32FlexIOConfig)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_44();
    /* Reset all registers */
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    REG_BIT_SET32(FLEXIO_CTRL_ADDR32,(FLEXIO_CTRL_SWRST_MASK_U32));
    /* Configure  */
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    REG_WRITE32(FLEXIO_CTRL_ADDR32,(u32FlexIOConfig & FLEXIO_CTRL_INIT_BITS_MASK_U32));
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    REG_RMW32(FLEXIO_CTRL_ADDR32, FLEXIO_CTRL_FLEXEN_MASK_U32, FLEXIO_MODULE_ENABLED_U32);
    
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_44();
}

FUNC (void, MCL_CODE) Mcl_Flexio_DeInit()
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_46();
    
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    REG_BIT_SET32(FLEXIO_CTRL_ADDR32,(FLEXIO_CTRL_SWRST_MASK_U32));
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    REG_WRITE32(FLEXIO_CTRL_ADDR32, (uint32)0x0);
    
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_46();
}

FUNC (void, MCL_CODE) Mcl_Flexio_ModuleEnable(void)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_35();
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    REG_RMW32(FLEXIO_CTRL_ADDR32, FLEXIO_CTRL_FLEXEN_MASK_U32, FLEXIO_MODULE_ENABLED_U32);
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_35();
}

FUNC (void, MCL_CODE) Mcl_Flexio_ModuleDisable(void)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_36();
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    REG_RMW32(FLEXIO_CTRL_ADDR32, FLEXIO_CTRL_FLEXEN_MASK_U32, FLEXIO_MODULE_DISABLED_U32);
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_36();
}

FUNC (void, MCL_CODE) Mcl_Flexio_ClrShiftStat(VAR(uint8, AUTOMATIC) u8mask)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_37();
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    FLEXIO_SHIFTSTAT_CLEAR(u8mask);
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_37();
}

FUNC (uint8, MCL_CODE) Mcl_Flexio_RdShiftStat(VAR(uint8, AUTOMATIC) u8mask)
{
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
   return FLEXIO_SHIFTSTAT_READ(u8mask);
}

FUNC (void, MCL_CODE) Mcl_Flexio_ClrShiftErr(VAR(uint8, AUTOMATIC) u8mask)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_38();
    
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    FLEXIO_SHIFTERR_CLEAR(u8mask);
    
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_38();
}

FUNC (uint8, MCL_CODE) Mcl_Flexio_RdShiftErr(VAR(uint8, AUTOMATIC) u8mask)
{
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    return FLEXIO_SHIFTERR_READ (u8mask);
}

FUNC (void, MCL_CODE) Mcl_Flexio_ClrTimStat(VAR(uint8, AUTOMATIC) u8mask)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_39();
    
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    FLEXIO_TIMSTAT_CLEAR(u8mask);
    
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_39();
}

FUNC (uint8, MCL_CODE) Mcl_Flexio_RdTimStat(VAR(uint8, AUTOMATIC) u8mask)
{
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    return FLEXIO_TIMSTAT_READ(u8mask);
}

FUNC (void, MCL_CODE) Mcl_Flexio_WrShiftSien(VAR(uint8, AUTOMATIC) u8ShifterMask, VAR(uint8, AUTOMATIC) u8ShifterEnableMask)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_40();
    
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_3 MISRA 2012 Advisory 13.4*/
    FLEXIO_SHIFTSIEN_SET(u8ShifterMask, u8ShifterEnableMask);
    
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_40();
}

FUNC (uint8, MCL_CODE) Mcl_Flexio_RdShiftSien(VAR(uint8, AUTOMATIC) u8ShifterMask)
{
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    return FLEXIO_SHIFTSIEN_READ(u8ShifterMask);
}

FUNC (void, MCL_CODE) Mcl_Flexio_WrShiftEien(VAR(uint8, AUTOMATIC) u8ShifterMask, VAR(uint8, AUTOMATIC) u8ShifterEnableMask)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_41();
    
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_3 MISRA 2012 Advisory 13.4*/
    FLEXIO_SHIFTEIEN_SET(u8ShifterMask, u8ShifterEnableMask);
    
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_41();
}

FUNC (uint8, MCL_CODE) Mcl_Flexio_RdShiftEien(VAR(uint8, AUTOMATIC) u8ShifterMask)
{
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    return FLEXIO_SHIFTEIEN_READ(u8ShifterMask);
}

FUNC (void, MCL_CODE) Mcl_Flexio_WrTimIen(VAR(uint8, AUTOMATIC) u8TimerMask, VAR(uint8, AUTOMATIC) u8TimerEnableMask)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_42();
    
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_3 MISRA 2012 Advisory 13.4*/
    FLEXIO_TIMIEN_SET(u8TimerMask, u8TimerEnableMask);
    
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_42();
}

FUNC (uint8, MCL_CODE) Mcl_Flexio_RdTimIen(VAR(uint8, AUTOMATIC) u8TimerMask)
{
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    return FLEXIO_TIMIEN_READ(u8TimerMask);
}

FUNC (void, MCL_CODE) Mcl_Flexio_WrShiftSden(VAR(uint8, AUTOMATIC) u8ShifterMask, VAR(uint8, AUTOMATIC) u8ShifterEnableMask)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_43();
    
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_3 MISRA 2012 Advisory 13.4*/
    FLEXIO_SHIFTSDEN_SET(u8ShifterMask, u8ShifterEnableMask);
    
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_43();
}

FUNC (uint8, MCL_CODE) Mcl_Flexio_RdShiftSden(VAR(uint8, AUTOMATIC) u8ShifterMask)
{
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    return FLEXIO_SHIFTSDEN_READ(u8ShifterMask);
}

FUNC (void, MCL_CODE) Mcl_Flexio_SetInterrupts(VAR(uint8, AUTOMATIC) u8ShifterMask, VAR(uint8, AUTOMATIC) u8ErrMask, VAR(uint8, AUTOMATIC) u8TimerMask)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_47();
    
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_3 MISRA 2012 Advisory 13.4*/
    FLEXIO_SHIFTSIEN_SET(u8ShifterMask, u8ShifterMask);
    
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_3 MISRA 2012 Advisory 13.4*/
    FLEXIO_SHIFTEIEN_SET(u8ErrMask, u8ErrMask);

    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_3 MISRA 2012 Advisory 13.4*/
    FLEXIO_TIMIEN_SET(u8TimerMask, u8TimerMask);
    
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_47();
}

FUNC (void, MCL_CODE) Mcl_Flexio_ClrInterrupts(VAR(uint8, AUTOMATIC) u8ShifterMask, VAR(uint8, AUTOMATIC) u8ErrMask, VAR(uint8, AUTOMATIC) u8TimerMask)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_48();
    
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_3 MISRA 2012 Advisory 13.4*/
    FLEXIO_SHIFTSIEN_SET(u8ShifterMask, 0U);

    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_3 MISRA 2012 Advisory 13.4*/
    FLEXIO_SHIFTEIEN_SET(u8ErrMask, 0U);

    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_3 MISRA 2012 Advisory 13.4*/
    FLEXIO_TIMIEN_SET(u8TimerMask, 0U);
    
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_48();
}

FUNC (void, MCL_CODE) Mcl_Flexio_SwReset(void)
{
    SchM_Enter_Mcl_MCL_EXCLUSIVE_AREA_45();
    
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    REG_BIT_SET32(FLEXIO_CTRL_ADDR32,(FLEXIO_CTRL_SWRST_MASK_U32));
    /* @violates @ref Mcl_FlexIO_Common_c_REF_1 MISRA 2012 Advisory 11.4*/
    /* @violates @ref Mcl_FlexIO_Common_c_REF_2 MISRA 2012 Required 11.6*/
    REG_BIT_CLEAR32(FLEXIO_CTRL_ADDR32,FLEXIO_CTRL_SWRST_MASK_U32);
    
    SchM_Exit_Mcl_MCL_EXCLUSIVE_AREA_45();
}
#define MCL_STOP_SEC_CODE
/* @violates @ref Mcl_FlexIO_Common_c_REF_4 MISRA 2012 Required 4.10*/
/* @violates @ref Mcl_FlexIO_Common_c_REF_5 MISRA 2012 Advisory 20.1*/
#include "MemMap.h"

#endif /*(MCL_ENABLE_FLEXIO == STD_ON)*/

#ifdef __cplusplus
}
#endif
/** @} */
