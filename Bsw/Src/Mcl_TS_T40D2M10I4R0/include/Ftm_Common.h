/**
*   @file    Ftm_Common.h
*   @version 1.0.4
*
*   @brief   AUTOSAR Mcl - Ftm driver header file.
*   @details Ftm driver interface.
*
*   @addtogroup FTM_MODULE
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.0 MCAL
*   Platform             : ARM
*   Peripheral           : eDMA
*   Dependencies         : none
*
*   Autosar Version      : 4.0.3
*   Autosar Revision     : ASR_REL_4_0_REV_0003
*   Autosar Conf.Variant :
*   SW Version           : 1.0.4
*   Build Version        : S32K14xS32K14x_MCAL_1_0_4_RTM_ASR_REL_4_0_REV_0003_20190307
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2018-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/

#ifndef FTM_COMMON_H
#define FTM_COMMON_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include   "CDD_Mcl_Cfg.h"
/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @{
* @file           Ftm_Common.h  
*/
#define FTM_COMMON_VENDOR_ID                       43
#define FTM_COMMON_AR_RELEASE_MAJOR_VERSION        4
#define FTM_COMMON_AR_RELEASE_MINOR_VERSION        0
#define FTM_COMMON_AR_RELEASE_REVISION_VERSION     3
#define FTM_COMMON_SW_MAJOR_VERSION                1
#define FTM_COMMON_SW_MINOR_VERSION                0
#define FTM_COMMON_SW_PATCH_VERSION                4
/** @} */

/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/

/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/

/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/

/*==================================================================================================
*                                            ENUMS
==================================================================================================*/

/*==================================================================================================
*                                 STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/

#define MCL_START_SEC_CONST_32
#include "MemMap.h"

#if (MCL_SELECT_COMMON_TIMEBASE_API == STD_ON)
FUNC(void, MCL_CODE) Mcl_Ftm_SelectCommonTimebase
(
    VAR (uint8, AUTOMATIC) u8FtmModule,
    VAR (uint16, AUTOMATIC) u16ElementSyncList
);
#endif /* MCL_SELECT_COMMON_TIMEBASE_API */

#define MCL_STOP_SEC_CODE
#include "MemMap.h"

#ifdef __cplusplus
}
#endif

#endif /* FTM_COMMON_H*/

/** @} */

